/**
 @classdesc Routes pour les jeux
 @author Kolly Florian
 @version 1.0
 **/

const textToSpeech = require('@google-cloud/text-to-speech');

/** Constante pour le model Response */
const Response = require('../models/Response.js');
/** Constante pour le fichier de traduction (mails)*/
const translations = require('./../translations.json');
const {body} = require("express-validator/check");

const clientTTS = new textToSpeech.TextToSpeechClient();

let currentExercice;

module.exports = (router, api, wrkModule) => {

    router.post('/sendDatatoLRS', (req, res) => {
        try {
            api.sendDatatoLRS(req.body.exoId,req.body.exoName).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    // res.write(JSON.stringify(response.data.data));
                    res.write(response.data);
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    /**
     * Fonction préalable à l'appel "/jeu/:jeu"
     *  permet de définir le mode (sans parcours / avec parcours)...
     */
    router.get('/run/:template/:exoId/:jeu/:gameMode', util.verifySession, (req, res) => {
        req.session.template = req.params.template;
        req.session.forcedExerciceId = req.params.exoId;
        req.session.gameMode = req.params.gameMode;
        res.writeHead(301,
            {Location: '/jeu/'+req.params.jeu}
        );
        res.end();
    });

    router.get('/jeu/:jeu', util.verifySession, (req, res) => {
        if (req.session.eleveID) {
            let eleve;
            let idExoLegram = null;
            api.getEleve(req.session.eleveID).then(response => {
                if (response.data.status === 'SUCCESS') {
                    eleve = response.data.data;
                    return api.getJeuByID(req.params.jeu);
                } else {
                    return res.redirect('/classes');
                }
            }).then(response => {
                if (response.data.status === 'SUCCESS') {
                    const jeu = response.data.data;
                    jeu.inputs = JSON.parse(jeu.json).inputs;
                    currentExercice = req.params.jeu;
                    req.session.gameID = currentExercice;
                    req.session.save(async (err) => {
                        if (err) {
                            return res.render('404', {error: 'La session n\'a pas pu être enregistrée'});
                        } else {
                            let module;
                            // Appel depuis RunParcours
                            if (req.session.template==='pixiParcours')
                                module =wrkModule.getModule('pixiParcours');
                            else if (req.session.template==='externalParcours') {
                                module = wrkModule.getModule('externalParcours');
                                idExoLegram = (await api.getExerciceById(req.session.forcedExerciceId)).data.data.exercice;
                            }
                            // Appel en mode jeu libre
                            else {
                                module = wrkModule.getModule(jeu.module);
                                idExoLegram = 'false';
                                req.session.forcedExerciceId = null;
                            }
                            if (module) {
                                return res.render(module.template, {
                                    jeu,
                                    eleve,
                                    gameMode:req.session.gameMode,
                                    parcours:req.session.parcours,
                                    forcedExerciceId:req.session.forcedExerciceId,
                                    idExoLegram,
                                    back:'runParcours',
                                    language: util.getLang(res)
                                });
                            } else {
                                // TODO: should not be a 404 error
                                return res.render('404', {error: 'Aucun module de rendu trouvé pour jeu.'});
                            }
                        }
                    });
                } else {
                    return res.render('404', {error: 'Impossible d\'obtenir le jeu'});
                }
            });
        } else {
            return res.redirect('/');
        }
    });

    router.post('/textToSpeech', async (req, res) => {
        const {text, languageCode} = req.body;
        const request = {
            input: {text},
            voice: {languageCode: languageCode, ssmlGender: 'NEUTRAL'},
            audioConfig: {audioEncoding: 'MP3'}
        };
        try {
            const [response] = await clientTTS.synthesizeSpeech(request);
            const data = JSON.parse(JSON.stringify(response.audioContent)).data;
            res.writeHead(200, {
                'Content-Type': 'audio/mpeg'
            });
            res.write(JSON.stringify(data));
            res.end();
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getSessionGameId', util.verifySession, (req, res) => {
        return new Response('success', 200, 'ok', false, {
            gameId : req.session.gameID
        }).send(res);
    });

    router.post('/getJeu', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        if (req.session.gameID) {
            const data = (await api.getJeuByID(req.session.gameID)).data;
            if (data.status === 'SUCCESS') {
                const game = data.data;
                game.inputs = JSON.parse(game.json).inputs;
                const dataEleve = (await api.getEleve(req.session.eleveID)).data;
                if (dataEleve.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].game_loaded, false, {
                        game,
                        degre: dataEleve.data.fk_degre,
                        language: util.getLang(res)
                    }).send(res);
                } else {
                    return new Response('error', 404, translations[currentLang].student_notfound, true).send(res);
                }
            } else {
                return new Response('error', 404, translations[currentLang].game_notfound, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    router.post('/getExercice', (req, res) => {
        const currentLang = util.getLang(res);
        try {
            api.getExercice(currentExercice, req.body.degre).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    // return new Response('error', 404, translations[currentLang].game_notfound, true).send(res);
                    return res.render('404', { error: 'impossible de charger la page des jeux' });
                    // Swal.fire({
                    //     title: 'error',
                    //     text: 'Timeout',
                    // });
                    // res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getExerciceById', (req, res) => {
        try {
            api.getExerciceById(req.body.forcedExerciceId).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getExercicesEleves', (req, res) => {
        try {
            api.getExerciceEleves(currentExercice, req.body.degre).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/addExercice', util.verifySession, async (req, res) => {
        let fk_jeu;
        const {exercice, degre, forcedState, gameId } = req.body;
        req.session.gameID ? fk_jeu = req.session.gameID : fk_jeu=gameId;
        const currentLang = util.getLang(res);
        if (fk_jeu) {
            const data = (await api.addExercice(exercice, fk_jeu, req.session.eleveID, degre, forcedState)).data;
            if (data.status === 'SUCCESS') {
                new Response('success', 200, translations[currentLang].exercice_add_success, false, {id: data.data}).send(res);
            } else {
                new Response('error', 500, data, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    /**
     * Ajoute une statistique par rapport au jeu et l'élève dans la session
     */
    router.post('/addStats', util.verifySession, async (req, res) => {
        const {mode,gameId,forcedExerciceId} = req.body;
        const currentLang = util.getLang(res);
        let parcoursId = null;
        if (req.session.parcours)
            parcoursId = req.session.parcours.id;
        // Si on ne passe pas par la page /run... (exp : unplugged Activity)
        // Todo passer par la page run ou mettre tout dans une fonction équivalente
        if (req.body.gameId){
            req.session.gameID = req.body.gameId;
            req.session.forcedExerciceId = req.body.forcedExerciceId;
            req.session.template = global.modeParcoursExternal;
        }

        if (req.session.eleveID && req.session.gameID) {
            // On récupère l'exercice courant pour l'ajouter aux stats
            let exo;
            let idExo = null;
            // Si on est en mode Parcours, on récupère l'exercice forcé
            if (req.session.template===global.modeParcoursPixi || req.session.template===global.modeParcoursExternal)
                exo = (await api.getExerciceById(req.session.forcedExerciceId)).data;
            // Sinon
            else
                exo = (await api.getExercice(req.session.gameID, 3)).data;

            if (exo.status === 'SUCCESS')
                idExo=exo.data.id;

            const data = (await api.addStats(mode, req.session.gameID, req.session.eleveID,idExo,parcoursId)).data;
            if (data.status === 'SUCCESS') {
                new Response('success', 200, translations[currentLang].stats_success, false, {id: data.data}).send(res);
            } else {
                new Response('error', 500, translations[currentLang].stats_error, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    /**
     * Mets à jour une statistique avec l'évaluation
     */
    router.post('/updateStats', util.verifySession, async (req, res) => {
        const {id, evaluation} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.updateStats(id, evaluation)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].stats_success).send(res);
        } else {
            new Response('error', 500, translations[currentLang].stats_error, true).send(res);
        }
    });

    /**
     * Signal un jeu faux
     */
    router.post('/signaler', util.verifySession, async (req, res) => {
        const {ex, msg, password} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.signaler(msg, password, ex, req.session.teacherID)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].signal_success).send(res);
        } else if (data.status === 'WARNING') {
            new Response('warning', 200, translations[currentLang].signal_bad_password).send(res);
        } else {
            new Response('error', 500, translations[currentLang].signal_error, true).send(res);
        }
    });

    router.post('/getSprites', (req, res) => {
        try {
            api.getSprites(req.body.gameId).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            res.status(404).send();
        }
    });
};
