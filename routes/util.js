/**
  @classdesc Classe comportant des méthodes utiles
  @author Kolly Florian, Vincent Audergon
  @version 1.0
**/

const md5 = require('md5');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('./../config.json');
/** Constante pour nodemailer */
const nodemailer = require('nodemailer');
/** Constante pour CSV */
const csv = require('fast-csv');
/** Constante pour le fichier de traduction (mails)*/
const translations = require('./../translations.json');

/**
 * Vérifie la session via le token
 * @param  {request} req [la requête faite par Express]
 * @return {boolean}     [true si la session est valide, false autrement]
 */
const verifySession = (req, res, next) => {
  let ok = false;
  if (req.session.token) {
    try {
      const decoded = jwt.verify(req.session.token, config.jwt.secret, { issuer: config.jwt.issuer, algorithms: [config.jwt.algorithm] });
      if (decoded !== undefined) ok = true;
    } catch (err) {
      ok = false;
    }
  }
  if (ok) next();
  else res.redirect('/');
};

/**
 * Crée et renvoi le salt de communication
 * @return {string} [le salt de communication]
 */
const getCommunicationSalt = () => md5(config.salt.password_partie_1 + _getDaysOfWeek() + config.salt.password_partie_2);

/**
 * Obtient le nom du jour pour le salt
 * @return {string} [le nom du jour en anglais]
 */
const _getDaysOfWeek = () => {
  const dayOfWeek = new Date().getDay();
  return isNaN(dayOfWeek) ? undefined : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
};

/**
 * Obtient la langue actuellement utilisée
 * @method getLang
 * @param  {resolve} res l'objet resolve
 * @return {string}      la langue en deux caractère
 */
const getLang = (res) => {
  const currentLang = res.cookie('lang').locale;
  // lors de la récupération de la valeur du cookie, il la reset et il faut donc la remettre
  res.cookie('lang', currentLang);
  return currentLang;
};

/**
 * Generate un code aléatoire avec length caractères
 * @return {string} le code généré
 */
const generateRandomCode = length => {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < length; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
};

//==========================================
//                Emails
//==========================================

/**
 * Tranporteur pour envoyer les messages avec nodemailer
 */
const transporter = nodemailer.createTransport({
  host: config.mail.host,
  port: 465,
  secure: true,
  auth: {
    user: config.mail.user,
    pass: config.mail.pass
  },
  tls: {
    rejectUnauthorized: false
  }
});

/**
 * Envoie un email. Retourne un booléen qui indique si l'email a bien été envoyé.
 * @param {string} to l'adresse email de destination
 * @param {string} subject l'objet de l'email
 * @param {string} text le contenu de l'email en texte brut
 * @param {string} html le contenu de l'email au format html
 * @param {string} lang la langue dans laquelle envoyer l'email
 * @param {array} attachments  Les pièces jointes à mettre avec l'email (par défaut un tableau vide)
 * @return un booleen qui indique si l'email a été envoyé
 */
const sendEmail = async (to, subject, text, html, lang, attachments = []) => {
  const options = {
    from: `${config.mail.user} <${config.mail.user}>`,
    to: to,
    subject: subject,
    text: text + `\n${translations[lang].email_end_1}\n${translations[lang].email_end_2}`,
    html: html + `<br>${translations[lang].email_end_1}<br><b>${translations[lang].email_end_2}</b><style>*{font-family: Arial}</style>`,
    attachments : attachments
  };
  let retour = false;
  await new Promise(resolve => {
    transporter.sendMail(options, err => {
      if (err) {
        console.log(err);
        resolve(false);
      }
      else {
        resolve(true);
      }
    });
  }).then((ok) => {
    retour = ok;
  });
  return retour;
};

//=======================================
//                CSV
//=======================================

/**
 * Transforme un tableau en fichier CSV
 * @param {array} records Le tableau d'objets à transfromer en CSV
 * @param {*} filename Le nom du fichier cible
 * @param {*} callback La méthode de callback (paramètres: string err => si il y a une erreur)
 */
const generateCSV = (records, filename, callback) => {
  if (records !== undefined) {
    if (filename.trim() !== '') {
      var ws = fs.createWriteStream(filename);
      return csv
        .write(records, { headers: true })
        .pipe(ws)
        .on('finish', callback);
    } else {
      callback('Filename is invalid');
    }
  }else{
    callback('Records array is undefined');
  }
};


module.exports = {
  verifySession,
  getCommunicationSalt,
  getLang,
  generateRandomCode,
  sendEmail,
  generateCSV
};
