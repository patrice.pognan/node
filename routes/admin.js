/**
 * @classdesc
 * @param router
 * @param api
 */


const Response = require('../models/Response.js');
const translations = require('./../translations.json');

module.exports = (router, api) => {

    router.get('/admin/valider', (req, res) => {

        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            api.getExericesForValidation().then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render('valider', {
                        exercices: JSON.parse(response.data.data).exercices,
                        degres: JSON.parse(response.data.data).degres,
                        classes: JSON.parse(response.data.data).classes,
                        games: JSON.parse(response.data.data).games,
                        lang: util.getLang(res)
                    });

                } else {
                    return res.render('404', {error: 'request was a failure'});

                }

            })  .catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

        router.post('/admin/valider/supprimer', util.verifySession, async (req, res) => {

        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {
            const currentLang = util.getLang(res);
            api.deleteExercice(req.body.exercice).then(response => {
                if (response.data.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].delete_exercice_success).send(res);
                } else {
                    return new Response('error', 404, translations[currentLang].delete_exercice_error).send(res);
                }
            }).catch((e) => {
                return new Response('error', 200, translations[currentLang].delete_exercice_error).send(res);
            });

        } else {
            return new Response('error', 403, translations[currentLang].delete_exercice_forbidden).send(res);
        }
    });

    router.get('/admin/valider/:gameId/exercice/:id/:back', (req, res) => {

        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            api.getExericesById(req.params.id).then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render('validation/exercice'+req.params.gameId, {
                        exercice: JSON.parse(response.data.data),
                        lang: util.getLang(res),
                        back: req.params.back,
                        id:req.params.id
                    });

                } else {
                    return res.render('404', {error: 'request was a failure'});

                }

            }).catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

    router.get('/admin/role', (req, res) => {

        if (req.session.teacherRole && req.session.teacherRole === 3) {

            api.getUsers().then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render('role', {users: response.data.data, currentTeacher:req.session.teacherID});

                } else {

                    return res.render('404', {error: 'request was a failure'});

                }

            }).catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

    router.get('/admin/signalement', (req, res) => {

        if (req.session.teacherRole && req.session.teacherRole === 3) {

            api.getExerciceSignale().then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render('signalement', {
                        exercices: response.data.data,
                        lang: util.getLang(res)
                    });

                } else {

                    return res.render('404', {error: 'request was a failure'});

                }

            }).catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

    router.post('/updateRole', util.verifySession, async (req, res) => {

        if (req.session.teacherRole && req.session.teacherRole === 3) {

            const data = (await api.updateRole(req.body.userId, req.body.role)).data.data;

            return new Response('success', 200, data).send(res);

        }
    });

    router.post('/updateExercice', util.verifySession, async (req, res) => {
        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            const data = (await api.updateExercice(req.body.exercice,req.body.levelInput,req.body.exId)).data.data;

            return new Response('success', 200, data).send(res);

        }
    });

    router.post('/updateExerciceState', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            const response = await api.updateExerciceState(req.body.description,req.body.state,req.body.levelInput,req.body.imageB64,req.body.exId);

            if (response.data.status === 'SUCCESS') {

                return new Response('success', 200, translations[currentLang].update_exerice_state_success).send(res);

            } else {

                return new Response('error', 200, translations[currentLang].update_exerice_state_error).send(res);

            }
        }
    });

    router.get('/getSalt', (req, res) => {
        return new Response('success',200, api.returnSalt()).send(res);
    });
};