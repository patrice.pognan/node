define(["require", "exports"], function (require, exports) {
    "use strict";
    var IMG = (function () {
        function IMG(base64, width, height, frames) {
            this.frames = frames;
            this.width = width / this.frames;
            this.height = height;
            this.image = new Image();
            this.image.onload = (function (that) {
                return function () {
                    that.loaded = true;
                };
            })(this);
            this.image.src = "data:image/png;base64," + base64;
        }
        IMG.prototype.getWidth = function () { return this.width; };
        IMG.prototype.getHeight = function () { return this.height; };
        IMG.prototype.getImage = function () { return this.image; };
        IMG.prototype.getFrames = function () { return this.frames; };
        IMG.prototype.isLoaded = function () { return this.loaded; };
        return IMG;
    }());
    return IMG;
});
