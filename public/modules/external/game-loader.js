let w = null;
let mode = 1;
let idStats = null;
let forcedExerciceId = null;

/**
 * Gets our iframe element.
 */
const getFrame = function () {
    return w || (w = document.getElementById('game'));
};

$(document).ready(function () {
    /**
     * Callbacks API
     */
    window.onmessage = function (e) {
        handleMessage(e.data);
    };
    wrk.send('/getJeu', true, {}, (data, success) => {
        if (success) {
            const {game} = data.data;
            const initialScript = game.script;
            let secondScript;
            let finalScript;
            if (forcedExerciceId){
                secondScript = initialScript.slice(0,-2);
                finalScript = secondScript+'&code='+forcedExerciceId+'\')';
                eval(finalScript);
            }
            // bootstrap the game
            else
            eval(initialScript);
        }
        else
            console.log('problème de chargement du jeu');
    });
});

/**
 * Loads a game
 **/
const loadGame = function (url) {
    const frame = document.getElementById('game');
    frame.src = url;
};

/**
 * Returns a data object.
 **/
const getMessageDataWrapper = function (type) {
    return {
        container: 'hep3-gameshub',
        type: type
    };
};

/**
 * Sends a message to the iframe.
 **/
const sendMessage = function (type, data) {
    getFrame().contentWindow.postMessage({...data, ...getMessageDataWrapper(type)}, '*');
};

/**
 * Handles a statistics submission.
 * We assume that the data type is already set to 'stats'
 **/
const handleStats = function (data) {

    /**+
     * Creates a new stats entry.
     */
    const createStatsEntry = function (mode) {
        $.ajax({
            type: 'POST',
            url: '/addStats',
            dataType: 'json',
            timeout: 2000,
            data: {
                mode: mode
            },

            success: function (xhr) {
                idStats = xhr.data.id;
            },
            error: function (xhr) {
                console.error('Create stats: Unable to create a stats entry');
            }
        });
    };

    /**+
     * Updates our current entry.
     */
    const updateStatsEntry = function (evaluation) {
        if (![0, 1, 2].includes(evaluation) || !idStats) {
            console.warn('Update stats: Wrong evaluation given or we don\'t have a stats ID yet.');
            return;
        }
        $.ajax({
            type: 'POST',
            url: '/updateStats',
            dataType: 'json',
            timeout: 2000,
            data: {
                id: idStats,
                evaluation: evaluation
            },

            success: function (xhr) {
                idStats = xhr.data.id;
                if (document.getElementById('parcours'))
                    document.getElementById('parcours').click();
            },

            error: function (xhr) {
                console.error('Update stats: Unable to update our stats entry.');
            }
        });
    };

    switch (data.event) {
        case 'start':
            createStatsEntry(data.mode);
            break;

        case 'end':
            updateStatsEntry(data.evaluation);
            break;

        default:
            break;
    }
};

/**
 * Handles messages coming from the child window.
 *
 * We currently support the following operations:
 * - back   -> we should go back to the games list
 * - degree -> set the selected degree
 * - stats  -> receive stats
 */
const handleMessage = function (data) {
    if (!data.type) {
        return;
    }

    switch (data.type) {
        case 'back':
            window.location = '/jeux';
            break;

        case 'degree':
            $('#currentDegree').html(data.value);
            break;

        case 'mode': {
            const value = data.value;
            const $el = $(`#mode-${value}`);
            $('#currentMode').html($el.html());
            break;
        }

        case 'stats':
            handleStats(data);
            break;

        default:
            break;
    }
};

/**
 * Raised by the iframe when the content has been loaded.
 **/
const onloaded = (nickname, degree, forcedExoId) => {
    if (forcedExoId!=='false') {
        forcedExerciceId = forcedExoId;
        mode=2;
    }
    /**
     * Global layout setup
     */
    sendMessage('setup', {
        type: 'setup',
        nickname: nickname,
        degree: degree,
        mode: mode
    });
};

/**
 * Handles when the degree value changes.
 */
const onDegreeChanged = function (value) {
    sendMessage('degree', {
        degree: value
    });
};

/**
 * Handles when the mode value changes.
 */
const onModeChanged = function (value) {
    if (mode === value) {
        return;
    }
    mode = value;
    sendMessage('mode', {
        mode: value
    });
};

/**
 * Handles when the degree value changes.
 */
const check = function (value) {
    sendMessage('degree', {
        degree: value
    });
};