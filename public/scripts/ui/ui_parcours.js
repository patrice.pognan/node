/**

 * @classdesc fonctionnalités pour la gestion des parcours

 * @author Aous Karoui

 * @version 1.0

 */

const HTMLNodes = {

    'nextNormalStep' : {
        'HTMLNode' : 'div',
        'attributes' : {
                'name' : 'globalStepDiv',
                'class': 'd-flex flex-column align-items-center m-2'
        },
        'children' :[
            {
                'HTMLNode' : 'div',
                'attributes' : {
                    'name' : 'stepContent',
                    'class': 'parcoursStep d-flex flex-column justify-content-around align-items-center'
                },
                'children' : [
                    {
                        'HTMLNode' : 'img',
                        'attributes' : {
                                'name' : 'stepImage',
                                'class': 'step-img',
                                'data-toggle': 'modal',
                                'data-target' : '#id-{exoId}'
                        }
                    },
                    {
                        'HTMLNode' : 'div',
                            'attributes' : {
                                'id' : 'stepLabel',
                                'name' : 'stepLabel',
                                'class': 'stepLabel d-flex flex-column align-items-center'
                            },
                            'children' : [
                                {
                                'HTMLNode' : 'div',
                                'attributes' : {
                                    'name' : 'nomJeu',
                                    'class' : 'text-info nom-jeu'
                                }
                                },
                                {
                                'HTMLNode' : 'a',
                                'attributes' : {
                                    'name' : 'niveau',
                                    'href' : '#',
                                    'data-toggle' : 'modal'
                                }
                                }
                            ]
                    },
                    {
                        'HTMLNode' : 'div',
                        'attributes' : {
                            'name' : 'trashDiv',
                            'class': 'mb-2 corbeille',
                            'onclick': 'ui_parcours.removeStep(this);'
                        },
                        'children' : {
                            'HTMLNode' : 'i',
                            'attributes' : {
                                'name' : 'data-feather',
                                'data-feather' : 'trash-2',
                                'width': '20px'
                            }
                        }
                    }
                ]
        },
        {
            'HTMLNode' : 'div',
            'attributes' : {
                'name' : 'boutonRemed',
                'class': 'boutonRemed',
                'title': 'Glisser ici la remédiation'
            },
            'children' : {
                'HTMLNode' : 'i',
                'attributes' : {
                    'name' : 'data-feather',
                    'data-feather' : 'rotate-cw'
                }
            }
        }
        ]
    },
    'dropNext' : {
        'HTMLNode' : 'span',
        'attributes' : {
            'name' : 'boutonNext',
            'class': 'boutonNext float-right mt-5 pl-2 pr-2',
            'title': 'Glisser ici l\'étape suivante'
        },
        'children' : {
            'HTMLNode' : 'i',
            'attributes' : {
                'name' : 'data-feather',
                'data-feather' : 'chevrons-right'
            }
        }
    },
    'chevronsDown' : {
        'HTMLNode' : 'i',
        'attributes' : {
            'name' : 'chevronsDown',
            'class': 'boutonRemed mt-2',
            'data-feather': 'chevrons-down'
        }
    },
    'basicModal' : {
        'HTMLNode' : 'div',
        'attributes' : {
            'class': 'modal fade',
            'aria-hidden': 'true'
        },
        'children' :[
            {
                'HTMLNode' : 'div',
                'attributes':{
                    'class' : 'modal-dialog'
                },
                'children' :[
                    {
                        'HTMLNode' : 'div',
                        'attributes':{
                            'class' : 'modal-content'
                        },
                        'children' :[
                            {
                                'HTMLNode' : 'div',
                                'attributes':{
                                    'class' : 'modal-header'
                                },
                                'children' :[
                                    {
                                        'HTMLNode' : 'img',
                                        'attributes':{
                                            'class' : 'col-2'
                                        }
                                    },
                                    {
                                        'HTMLNode' : 'h5',
                                        'attributes':{
                                            'class' : 'modal-title',
                                        }
                                    },
                                    {
                                        'HTMLNode' : 'a',
                                        'attributes':{
                                            'class' : 'closeButton',
                                            'href' : '#',
                                            'data-dismiss' : 'modal'
                                        }
                                    }
                                ]
                            },
                            {
                                'HTMLNode' : 'div',
                                'attributes':{
                                    'class' : 'modal-body'
                                }
                            },
                            {
                                'HTMLNode' : 'div',
                                'attributes':{
                                    'class' : 'modal-footer'
                                },
                                'children' : [
                                    {
                                        'HTMLNode' : 'button',
                                        'attributes' : {
                                            'type' : 'button',
                                            'class' : 'btn btn-secondary',
                                            'data-dismiss' : 'modal'
                                        }
                                    },
                                    {
                                        'HTMLNode' : 'button',
                                        'attributes' : {
                                            'class' : 'btn btn-outline-success'
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },
    'unpluggedModalContent' : {
        'HTMLNode' : 'form',
        'children' : [
            {
                'HTMLNode' : 'div',
                'attributes' : {
                    'class' : 'form-group row'
                },
                'children' : [
                    {
                        'HTMLNode' : 'label',
                        'attributes' : {
                            'class' : 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode' : 'input',
                            'attributes' : {
                                'type' : 'text',
                                'class' : 'col-8 form-control',
                                'role' : 'name',
                                'required':''
                            }
                    }
                ]
            },
            {
                'HTMLNode' : 'div',
                'attributes' : {
                    'class' : 'form-group row'
                },
                'children' : [
                    {
                        'HTMLNode' : 'label',
                        'attributes' : {
                            'class' : 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode' : 'textarea',
                        'attributes' : {
                            'type' : 'text',
                            'class' : 'col-8 form-control',
                            'role' : 'consigne',
                            'required':''
                            }
                    }
                ]
            },
            {
                'HTMLNode' : 'div',
                'attributes' : {
                    'class' : 'form-group row'
                },
                'children' : [
                    {
                        'HTMLNode' : 'label',
                        'attributes' : {
                            'class' : 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode' : 'textarea',
                        'attributes' : {
                            'type' : 'text',
                            'class' : 'col-8 form-control',
                            'role' : 'description'
                            }
                    }
                ]
            }
        ]
    }
};
const _const = {

    HTMLButton: 'button',
    HTMLDiv : 'div',
    HTMLH5 : 'H5',
    HTMLI : 'i',
    HTMLId : 'id',
    HTMLImg : 'img',
    HTMLInput : 'input',

    cancel : 'Annuler',
    validate : 'Valider',

    order : 'Consigne',

    remediationStep : 'remediation',
    normalStep : 'next',

    unpluggedExercice : 'unpluggedExercice',
    externalLink : 'externalLink',
    unpluggedExerciceDefaultId : 'unpluggedExerciceDefaultId',
    unpluggedActivityId : '16',
    externalLinkId : '18'
};

const json_per_brut=`{ "disciplines": [
        {
            "id": 1,
            "nom": "Français",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 1,
                            "nom": "Lire et écrire des textes d'usage familier et scolaire et s'approprier le système de la langue écrite",
                            "code": "L1 11-12",
                            "cycle": 1
                        },
                        {
                            "id": 3,
                            "nom": "Lire de manière autonome des textes variés et développer son efficacité en lecture",
                            "code": "L1 21",
                            "cycle": 2
                        },
                        {
                            "id": 7,
                            "nom": "Lire et analyser des textes de genres différents et en dégager les multiples sens",
                            "code": "L1 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        },
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        },
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 2,
                            "nom": "Comprendre et produire des textes oraux d'usage familier et scolaire",
                            "code": "L1 13-14",
                            "cycle": 1
                        },
                        {
                            "id": 5,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 23",
                            "cycle": 2
                        },
                        {
                            "id": 5,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 23",
                            "cycle": 2
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 2,
                            "nom": "Comprendre et produire des textes oraux d'usage familier et scolaire",
                            "code": "L1 13-14",
                            "cycle": 1
                        },
                        {
                            "id": 6,
                            "nom": "Produire des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 24",
                            "cycle": 2
                        },
                        {
                            "id": 10,
                            "nom": "Produire des textes oraux de genres différents adaptés aux situations d'énonciation",
                            "code": "L1 34",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 5,
                    "nom": "Accès à la littérature",
                    "objectifs":[
                        {
                            "id": 11,
                            "nom": "Apprécier des ouvrages littéraires",
                            "code": "L1 15",
                            "cycle": 1
                        },
                        {
                            "id": 15,
                            "nom": "Conduire et apprécier la lecture d'ouvrages littéraires",
                            "code": "L1 25",
                            "cycle": 2
                        },
                        {
                            "id": 19,
                            "nom": "Apprécier et analyser des productions littéraires diverses",
                            "code": "L1 35",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 12,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L1 16",
                            "cycle": 1
                        },
                        {
                            "id": 16,
                            "nom": "Construire une représentation de la langue pour comprendre et produire des textes",
                            "code": "L1 26",
                            "cycle": 2
                        },
                        {
                            "id": 20,
                            "nom": "Analyser le fonctionnement de la langue et élaborer des critères d'appréciation pour comprendre et produire des textes",
                            "code": "L1 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 13,
                            "nom": "Identifier l'organisation et le fonctionnement de la langue par l'observation et la manipulation d'autres langues",
                            "code": "L 17",
                            "cycle": 1
                        },
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 8,
                    "nom": "Écriture et instruments de la communication",
                    "objectifs":[
                        {
                            "id": 14,
                            "nom": "Découvrir et utiliser la technique de l'écriture et les instruments de la communication",
                            "code": "L1 18",
                            "cycle": 1
                        },
                        {
                            "id": 18,
                            "nom": "Utiliser l'écriture et les instruments de la communication pour planifier et réaliser des documents",
                            "code": "L1 28",
                            "cycle": 2
                        },
                        {
                            "id": 153,
                            "nom": "Exploiter l'écriture et les instruments de la communication pour collecter l'information, pour échanger et pour produire les documents",
                            "code": "L1 38",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 3,
            "nom": "Allemand",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 21,
                            "nom": "Lire des textes propres à des situations familières de communication",
                            "code": "L2 21",
                            "cycle": 2
                        },
                        {
                            "id": 25,
                            "nom": "Lire de manière autonome des textes rédigés en langage courant",
                            "code": "L2 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 22,
                            "nom": "écrire des textes simples propres à des situations familières de communication",
                            "code": "L2 22",
                            "cycle": 2
                        },
                        {
                            "id": 27,
                            "nom": "écrire des textes variés sur des sujets familiers ou d'intérêt personnel",
                            "code": "L2 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 23,
                            "nom": "Comprendre des textes oraux brefs propres à des situations familières de communication",
                            "code": "L2 23",
                            "cycle": 2
                        },
                        {
                            "id": 33,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 26",
                            "cycle": 2
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 24,
                            "nom": "Produire des énoncés simples propres à des situations familières de communication",
                            "code": "L2 24",
                            "cycle": 2
                        },
                        {
                            "id": 34,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 33,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 26",
                            "cycle": 2
                        },
                        {
                            "id": 34,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 4,
            "nom": "Anglais",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 173,
                            "nom": "Lire des textes propres à des situations familières de communication",
                            "code": "L3 21",
                            "cycle": 2
                        },
                        {
                            "id": 26,
                            "nom": "Lire de manière autonome des textes rédigés en langage courant",
                            "code": "L3 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 175,
                            "nom": "écrire des textes simples propres à des situations familières de communication",
                            "code": "L3 22",
                            "cycle": 2
                        },
                        {
                            "id": 180,
                            "nom": "écrire des textes variés sur des sujets familiers ou d'intérêt personnel",
                            "code": "L3 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 172,
                            "nom": "Comprendre des textes oraux brefs propres à des situations familières de communication",
                            "code": "L3 23",
                            "cycle": 2
                        },
                        {
                            "id": 30,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L3 33",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 174,
                            "nom": "Produire des énoncés simples propres à des situations familières de communication",
                            "code": "L3 24",
                            "cycle": 2
                        },
                        {
                            "id": 32,
                            "nom": "Produire des textes oraux variés propres à des situations de la vie courante",
                            "code": "L3 34",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 176,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L3 26",
                            "cycle": 2
                        },
                        {
                            "id": 181,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L3 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 2,
            "nom": "Latin",
            "competences": []
        }
    ]
}`;

const json_per_parsed = JSON.parse(json_per_brut);

// import konst from './ui_const.js';
// const a = require('./../../../models/json_per');

var ui_parcours = (function () { // eslint-disable-line no-unused-vars

    const PARCOURS_PRIVATE=0;
    const PARCOURS_PENDING=1;
    const PARCOURS_PUBLIC=2;
    const competences_div = $('#competences');
    const objectifs_div = $('#objectifs');
    const nomParcours = $('#nomParcours');
    const licence = $('#licence option:selected').text();
    const degre = $('#degre');
    const description = $('#description');
    // Un tableau de chaines JSON pour enregister les tomporairement les étapes du parcours (avant la sauvegarde en bdd)
    var globalStepsArray=[];
    var gameList;

    var nb_steps = 1;
    // Si false créer un parcours dans la bdd sinon faire un update (pour éviter le check de bdd à chaque click sur le bouton de sauvegarde)
    var parcours_created = false;

    // Création des éléments graphiques (arrow + boutons)
    const hiddenArrow = '<div class="mt-5 pl-2 pr-2 invisible"><i data-feather="arrow-right"></i></div>';
    const upRightArrow = '<div class="nextArrow"><i data-feather="trending-up" style="width: 5px; height: 5px;"></i></div>';
    const dropNext = '<span class="boutonNext float-right mt-5 " title="Glisser ici l\'étape suivante"><i data-feather="chevrons-right"></i></span>';
    const cornerRightUp = '<span title="Relier à l\'étape suivante"><i id="animatedArrow" data-feather="corner-right-up" onclick="ui_parcours.moveStep($(this))"></i></span>';
    const dropRemed = '<div class="boutonRemed" title="Glisser ici la remédiation"><i data-feather="rotate-cw"></i></div>';
    const stepsText = '<div class="texte-vertical">Étapes</div><div class="texte-vertical">obligatoires</div>';
    const remedText = '<div class="texte-vertical ml-3 mr-2">Remédiations</div>';
    const debranchDetails = `<!-- The modal -->
<!-- Modal -->
<div class="modal fade" id="flipFlop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                   <div class="modal-dialog">
                                     <div class="modal-content">
                                       <div class="modal-header">
                                         <h5 class="modal-title" id="staticBackdropLabel">Consigne</h5>
                                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">x</button>
                                       </div>
                                       <div class="modal-body">
                                         <input type="text" class="form-control form-control-lg mt-5 mb-5" placeholder="Écrire ici la consigne de l'activité debranchée" id="stepNameInput">
                                       </div>
                                       <div class="modal-footer">
                                         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                         <button type="button" class="btn btn-primary">Valider</button>
                                       </div>
                                     </div>
                                   </div>
                                 </div>`;

/************** Fonctionnalités de la page d'accueil 'classes.ejs' **************/

    const afficherCompetences = function (discipline_id,lien) {
         $('.discipline').removeClass('selected');
         lien.classList.add('selected');
        competences_div.html('');
        objectifs_div.html('');
        competence_id=0;
        discipline=discipline_id;
        json_per_parsed.disciplines[discipline_id].competences.forEach((competence) => {
            competences_div.append('<ul className="list-group fade">');
            competences_div.append('<span class="list-group-item objectif" onclick="ui_parcours.afficherObjectifs('+discipline+','+competence_id+',this)">'+competence.nom+'</span>');
            competences_div.append('</ul>');
            competence_id++;
         })
    };

    /**
     * Fonction pour masquer/afficher ...
     * @param  {string} elt bouton déclencheur
     */
    const toggleParcoursPER = function(elt){
        ;
    }

    const setParcours = function (elt){
        elt.parentNode.nextSibling.classList.add('selected');
    }
    const unSetParcours = function (elt){
        elt.parentNode.nextSibling.classList.remove('selected');
    }

    const afficherObjectifs = function (discipline_id,competence_id,lien) {
        $('.objectif').removeClass('selected');
        lien.classList.add('selected');
        objectifs_div.html('');
        json_per_parsed.disciplines[discipline_id].competences[competence_id].objectifs.forEach((objectif) => {
            objectifs_div.append('<ul class="list-group fade">');
            objectifs_div.append('<a href="ajouterParcours/999/0/undefined_name/'+json_per_parsed.disciplines[discipline_id].nom+'/'+json_per_parsed.disciplines[discipline_id].competences[competence_id].nom+'/'+objectif.code+'/'+objectif.nom+'"><i data-feather="plus-circle" style="position: relative; left: 110px; color: #17a2b8;" onmouseover="ui_parcours.setParcours(this)" onmouseleave="ui_parcours.unSetParcours(this)" ></i></a><div>'+objectif.nom+'<br>'+objectif.code+'</div>');
            objectifs_div.append('</ul>');
         })
        feather.replace();
    };

    const afficherAttente = function (lien){
        $('.attente').removeClass('selected');
        lien.classList.add('selected');
        if (window.location.href==='http://localhost:55555/scenario' || window.location.href==='http://localhost:55555/scenario#')
            setTimeout(function(){
                objectifs_div.html('');
            }, 1000);
    };

    /**
     * Fonction pour dupliquer un parcours public sur le compte d'un professeur
     * @param  {string} parcoursPrives liste des parcours privés du professeur
     * @param  {string} duplicatedParcours On envoie tout l'objet parcours
     */
    const duplicatePublicParcours = function (parcoursPrives,duplicatedParcours){
        // Tout d'abord, on change le nom pour le distinguer du parcours original
        duplicatedParcours.nom+=' (parcours public copié)';
        ui_global.showConfirmAlert('question','Télécharger ce parcours','Voulez-vous vraiment télécharger ce parcours ?','Télécharger',function (){
            wrk.send('/saveParcours', true, {
                competences:JSON.parse(duplicatedParcours.competences),
                stepsJson:JSON.parse(duplicatedParcours.steps),
                nomParcours:duplicatedParcours.nom,
                licence:duplicatedParcours.licence,
                degre:duplicatedParcours.degre,
                description:duplicatedParcours.description,
                requestURL:'saveParcours'
            }, function (data, success) {
                if (success) {
                    parcours_created = true;
                    // On met à jour l'affichage du tableau des parcours privés pour éviter un reload de la BDD
                    duplicatedParcours.steps=JSON.parse(duplicatedParcours.steps);
                    parcoursPrives.push(duplicatedParcours);
                    ui_parcours.loadParcoursByUser(parcoursPrives,'listeParcoursPrives');
                }
            }, true);
        });
    }

/*********** Fonctionnalités de la page de scénarisation 'ajouterParcours.ejs' ***********/

    /**
     * Ajoute une nouvelle étape du parcours
     * @param  {string} stepLevel Le nom du parcours/niveau
     * @param  {string} nomJeuBrut Le nom du jeu avant le formatage
     * @param  {string} stepType Le type de l'étape ('next', 'remed' ou 'newRemed')
     * @param  {number} idJeu l'id du jeu
     * @param  {string} stepId l'id du de l'étape
     * @param  {string} gameImage l'image du jeu
     */
    const addNewStep = function (stepLevel,nomJeuBrut,stepType,idJeu, stepId, gameImage){

        if (stepId===undefined)
            stepId='undefined'+nb_steps;

        // On regarde si l'étape n'a pas déjà été créée
        if (ui_parcours.checkStep(stepId)) return;

        // 1) On crée un json avec les infos du step
        const stepJson = createStepJson('',nb_steps,stepLevel,nomJeuBrut,idJeu,stepType,stepId);
        // 2) On ajoute le JSON de cette étape dans le tableau global des JSON des étapes du scénario
        globalStepsArray.push(stepJson);

        const step= ui_parcours.createStep(nomJeuBrut,dropNext,stepLevel,'next',stepId,idJeu,gameImage);

        // Même si le type est normal, on crée une remédiation invisible (pour des raisons graphiques) qui sera ensuite remplacée par la vraie remédiation
        const remed= ui_parcours.createStep(nomJeuBrut,dropNext,stepLevel,'remed',stepId,idJeu,gameImage);
        // Si c'est la première étape
        if (nb_steps===1){
            // Gestion graphique :
            // Masquer le texte de bienvenue
            $('#empty_parcours_welcome_text').html('');
            // Afficher le texte info
            $('#InfoDragAndDrop').removeClass('invisible');
            // Afficher le bouton de sauvegarde
            $('#updateParcours').removeClass('invisible');
            $('#firstSteps').append(stepsText);
            $('#remediations').append(remedText);
        }
        else{
            // j'utilise hiddenArrow pour conserver les dimensions d'espacement
            $('#remediations').append(hiddenArrow);
        }
        const dropNextNode = ui_global.createHTMLnode(HTMLNodes.dropNext.HTMLNode,HTMLNodes.dropNext.attributes,null,HTMLNodes.dropNext.children);
        $('#firstSteps').append(step);
        $('#firstSteps').append(dropNextNode);
        $('#remediations').append(remed);
        ui_parcours.makeItDroppable('boutonNext',stepLevel);
        ui_parcours.makeItDroppable('boutonRemed',stepLevel);
        feather.replace();
        $('#steps').css('backgroundColor','antiquewhite');
        nb_steps++;
        // console.log(globalStepsArray);
    };

    /**
     * Vérifie si l'étape n'existe pas déjà
     * @param  {string} stepId L'id de l'étape
     */
    const checkStep = function (stepId){
        if (document.getElementById(stepId)!==null){
            //alert('Attention, cette étape existe déjà dans le parcours');
            return true;
        }
        return false;
    }

    /**
     * Enregistre les informations d'une étape dans un JSON
     * @param  {string} defaultRemediationId L'id de la remédiation par défaut
     * @param  {number} stepNumber Le numéro du Step Parent
     * @param  {string} stepLevel Le niveau de la remédiation
     * @param  {string} gameFullName Le texte du nom du jeu à raccourcir par manque d'espace
     * @param  {number} idJeu l'id du jeu
     * @param  {string} stepType le type de l'étape (normale ou remediation)
     * @param  {int} stepId l'identifiant de l'étape
     */
    const createStepJson = function(defaultRemediationId,stepNumber,stepLevel, gameFullName,idJeu, stepType,stepId){
        const stepJson={};
        stepJson.number=stepNumber;
        stepJson.id = stepId;
        // todo : à changer par le chargement à partir de la BD (idéalement le json complet)
        if (stepLevel==='Debra' && stepId !== _const.unpluggedExercice)
                stepJson.consigne = $('#inputConsigne' + stepId).val();

        stepJson.niveau = stepLevel;
        // Utile pour l'affichage dans la page des parcours (nom complet avec espaces et accents)
        stepJson.fullName = gameFullName;
        // Utile pour faire des recherches en comparant avec d'autres chaines (sans espaces et accents)
        stepJson.shortName = normalizeString(gameFullName);
        stepJson.gameId = idJeu;
        stepJson.type = stepType;
        // Indique l'étape qui appelle la remédiation
        stepJson.defaultRemediationId = defaultRemediationId;
        // On retourne le JSON avec les infos de l'étape
        return stepJson;
    }

    /**
     * Crée graphiquement l'étape en fonction de son type (next, remed, newRemed)
     * (si c'est une étape normale (non remédiation), elle crée une remédiation invisible par défaut pour des raisons graphiques. Celle-ci sera ensuite remplacée par la vraie remédiation)
     */
    const createStep = function(nomJeu,dropNext,niveau,stepType,stepId,gameId,gameImage){

        if (stepType!=='next'){
            stepId ='remed'+stepId;
            // Si c'est une remédiation par défaut
            if (stepType==='remed'){
                const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode,HTMLNodes.nextNormalStep.attributes,null,HTMLNodes.nextNormalStep.children);
                initialStepNode.classList.add('invisible');
                // On enlève le bouton dropRemed
                initialStepNode.children[1].remove();
                const finalStepNode = fillStepInformation(initialStepNode,stepId,nomJeu,niveau,gameImage,gameId);
                return finalStepNode;
            }
            // Si c'est une remédiation insérée (newRemed)
            else{
                const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode,HTMLNodes.nextNormalStep.attributes,null,HTMLNodes.nextNormalStep.children);
                // On enlève le bouton dropRemed
                initialStepNode.children[1].remove();
                const finalStepNode = fillStepInformation(initialStepNode,stepId,nomJeu,niveau,gameImage,gameId);
                return finalStepNode;
            }
        }
        // Insertion d'une étape suivante normale (non remédiation)
        else{
            const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode,HTMLNodes.nextNormalStep.attributes,null,HTMLNodes.nextNormalStep.children);
            // On enlève le bouton dropRemed
            if (gameId === _const.unpluggedActivityId || gameId === _const.externalLinkId)
                initialStepNode.children[1].remove();
            const finalStepNode = fillStepInformation(initialStepNode,stepId,nomJeu,niveau,gameImage,gameId);
            return finalStepNode;
        }
    };

    const makeItDroppable = function (classElement,textElement){
        // On restreint le droppable aux deux boutons NextStep et Remediation (portant la classe 'boutonNext' ou 'boutonRemed')
        $('.'+classElement).droppable({
            drop: function(event, ui) {
                if (classElement==='boutonNext'){
                    const id = $(this).parent().attr('id');
                    if (id.search('remed')!==0)
                        ui_parcours.addNewStep(ui.draggable.attr('stepLevel'),ui.draggable.attr('role'),'next',ui.draggable.attr('gameId'),ui.draggable.attr("stepId"),ui.draggable.attr("gameImage"));
                    else
                        ui_parcours.moveStep(this);
                }
                else{
                    ui_parcours.addRemediation($(this).parent().attr('id'),ui.draggable.attr("stepLevel"),ui.draggable.attr("role"),ui.draggable.attr("gameId"),ui.draggable.attr("stepId"),ui.draggable.attr("gameImage"));
                }
            },
            over: function(event, ui) {
                $(this).css('background', 'lightgreen');
                if (classElement==='boutonNext'){
                    $('#draggedLevel').text('Étape Suivante');
                    $('#draggedLevel').animate({ left: "+=25"},{duration: 800});
                }
                else{
                    $('#draggedLevel').text('Remédiation');
                    $('#draggedLevel').animate({ top: "+=25"},{duration: 800});
                    $(this).children().replaceWith('<i data-feather="chevrons-down"></i>');
                    feather.replace();
                }
            },
            out: function(event, ui) {
                $(this).css('background', 'lightgray');
                $('#draggedLevel').text(ui.draggable.attr('stepLevel'));
                if (classElement==='boutonNext'){
                    ;
                }
                else{
                    $(this).children().replaceWith('<i data-feather="rotate-cw"></i>');
                }
                feather.replace();
            }
        });
    }

    /**
     * Pour insérer la remédiation
     * @param  {string} defaultRemediationId L'id de la remédiation par défaut
     * @param  {number} steNumber Le numéro du Step Parent
     * @param  {string} stepLevel Le niveau de la remédiation
     * @param  {string} nomJeuBrut Le texte du nom du jeu à raccourcir par manque d'espace
     * @param  {number} idJeu l'id du jeu
     * @param  {number} stepId l'id du step
     */
    const addRemediation = function (defaultRemediationId,stepLevel, nomJeuBrut,idJeu,stepId,gameImage){
        const parentStepNumber =
            getParentStepNumber(defaultRemediationId)+'.1';
        // Création stepJson (idem que dans AddNewStep)
        const stepJson = createStepJson(defaultRemediationId,parentStepNumber,stepLevel,nomJeuBrut,idJeu,'remediation',stepId);
        globalStepsArray.push(stepJson);
        // Gestion graphique
        const remediation= ui_parcours.createStep(nomJeuBrut,dropNext,stepLevel,'newRemed',stepId,idJeu,gameImage);
        $('#remed'+defaultRemediationId).replaceWith(remediation);
        document.getElementById(defaultRemediationId).children[1].replaceWith(ui_global.createHTMLnode(HTMLNodes.chevronsDown.HTMLNode, HTMLNodes.chevronsDown.attributes,null,null));
        feather.replace();
        ui_parcours.makeItDroppable('boutonNext',stepLevel);
    }

    /**
     * Fonction qui retourne le numéro de l'étape parent (utile pour placer la remédiation)
     * @param  {string} parentStepId L'id de la remédiation par défaut
     */
    const getParentStepNumber = function (parentStepId){
        let parentStepNumber=0;
        globalStepsArray.forEach((stepJson)=>{
            if (stepJson.id === parentStepId){
                parentStepNumber = stepJson.number;
            }
            else if (stepJson.number == parentStepId){
                parentStepNumber = stepJson.number;
            }
        });
        return parentStepNumber;
    }

    /**
     * Supprime entièrement une étape du parcours
     * @param  {string} target précise si la cible est un élève ou une classe
     * @param  {string} targetId id de la cible
     * @param  {string} clickedName nom de la cible
     */
    const removeStep = function (clickedButton){
        if (confirm('Voulez-vous vraiment supprimer cette étape ?')===true){
            const stepNode = clickedButton.parentNode.parentNode;
            const stepId = stepNode.getAttribute('id');
            const stepNumber = stepNode.getAttribute('number');

            // supprime la flèche
            if (stepNode.nextSibling!==null)
                stepNode.nextSibling.remove();
            // supprime la remédiation du globalStepsArray
            removeStepFromGlobalStepsArray(stepId,stepNumber);
            // supprime graphiquement la remédiation
            stepNode.remove();
            document.getElementById('firstSteps').innerHTML='';
            document.getElementById('remediations').innerHTML='';
            nb_steps=1;
            loadParcourSteps(JSON.stringify(globalStepsArray,null));
        }
    }

    /**
     * Supprime l'étape du parcours depuis le globalStepsArray
     * @param  {string} stepId l'id du step à supprimer
     * @param  {string} stepNumber le numéro de l'étape dans le GlobalStepsArray
     */
    const removeStepFromGlobalStepsArray = function (stepId,stepNumber){
        // On décrémente car le globalStepsArray commence à 0
        let local_nb_steps = stepNumber-1;
        let stepsArraylength = local_nb_steps;
        if (stepId.indexOf('remed'))
            stepsArraylength = stepNumber;
        // On incrémente s'il y a une remédiation
        for (var k = 0; k<stepsArraylength; k++) {
            if (globalStepsArray[k].type==='remediation') {
                local_nb_steps++;
                stepsArraylength++;
            }
        }
        // Si l'étape est liée à une remédiation, on supprime également la remédiation
        if (globalStepsArray[local_nb_steps].type==='next'){
            if (!document.getElementById('remed'+stepId)){
                const remedId = globalStepsArray[local_nb_steps+1].id;
                document.getElementById('remed'+remedId).remove();
                globalStepsArray.splice(local_nb_steps+1,1);
            }
        }
        globalStepsArray.splice(local_nb_steps,1);
        // Décalage des step.number dans le globalStepsArray
        for (var i = local_nb_steps; i<globalStepsArray.length;i++) {
            if (globalStepsArray[i].type==='next'){
                globalStepsArray[i].number = globalStepsArray[i].number-1;
            }
            else if (globalStepsArray[i].type==='remediation'){
                globalStepsArray[i].number = (parseFloat(globalStepsArray[i].number)-1).toFixed(1);
            }
        }
    }

    const moveStep = function (elt){
            if (confirm('Voulez-vous relier la remédiation à cette étape ?')){
                const parent = elt.parent().parent().parent().parent();
                elt.parent().css('width','22px');
                parent.before(upRightArrow);
                parent.prev().css('position','relative');
                parent.prev().css('left','110%');
                parent.prev().css('bottom','30px');
                // Pour éviter trop d'espacement au-dessus de la remédiation, je mets la hauteur du conteneur de l'Arrow à une petite échelle
                parent.prev().css('height','5px');
                parent.prev().children().css('width','40px');
                parent.prev().children().css('height','40px');
                elt.remove();
                feather.replace();
            }
            else{
                elt.removeAttr('style');
            }
    }

    /**
     * Fonction pour enlever les espaces et les accents
     */
    const normalizeString = function(string){
        // enlève les espaces
        let result = string.replace(/ /g,'');
        result = result.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
        return result;
    };

    // Todo apparemment fonction inutile (vérifier avant de l'enlever)
    const changeText = function (label){
        $('#stepLabel').html('');
        $('#stepLabel').append('<input type="text" name="classeName" id="classeName" class="form-control form-control-lg"\n' +
            '                       placeholder="nom étape" required autofocus>');
    };

    const updateOverflow = function(value){
        $('.niveaux-jeu').css('overflow', value);
    };

    /**
     * Fonction pour la création ou m.à.j d'un parcours
     * @param  {string} requestURL Ce paramètre détermine s'il y a un insert ou un update (c'est l'id du bouton cliqué)
     */
    const saveParcours = function (requestURL,competence){
        event.preventDefault();
        // Pour des raisons d'affichage, on trie le globalStepsArray avant de sauvegarder
        const sortedData = globalStepsArray.sort(function (a, b) {
            return a.number - b.number;
        });
        $('#parcoursCreationLabel').text('Mon parcours');
            wrk.send('/saveParcours', true, {
                stepsJson:sortedData,
                nomParcours:nomParcours.val(),
                competences:competence,
                licence:$('#licence option:selected').text(),
                degre:degre.val(),
                description:description.val(),
                requestURL:requestURL
            }, function (data, success) {
                if (success) {
                    parcours_created = true;
                    // Todo Essayer avec generatedKey après l'insertion (voir exemple addExercice dans Wrk.java)
                    wrk.send('/getSingleParcoursByUserAndName', true, {
                        nomParcours:nomParcours.val()
                    }, function (data, success) {
                        $('#steps').css('backgroundColor','#ddfad7');
                    }, true);
                }
            }, true);
    };

    /**
     * Fonction pour supprimer un parcours pour un élève ou une classe
     * @param  {string} target précise si la cible est un élève ou une classe
     * @param  {string} targetId id de la cible
     * @param  {string} clickedName nom de la cible
     */
    const deleteParcoursFrom = function (target, targetId, clickedName){
        wrk.send('/deleteParcoursFrom', true, {
            target:target,
            targetId: targetId,
            clickedName: clickedName
        }, function (data, success) {
            if (success) {
                console.log('parcours suuprimé');
            }
        }, true);
    };

    const showTagInput = function (){
        $('#tag_label').html('');
        $('#tag_label').append('<input type="text" placeholder="Ajouter un tag"/>');
    };

    /**
     * Affiche une image de prévisualisation d'un niveau
     * @param  {object} elt l'élément ciblé
     */
    const showLevelPreview = function (elt){
        if (elt.getAttribute('aria-label')){
            const img = document.createElement('img');
            img.setAttribute('src',elt.getAttribute('aria-label'));
            if (elt.nextSibling===null)
                elt.parentNode.appendChild(img);
        }
    };

    /**
     * Masque l'image de prévisualisation d'un niveau
     * @param  {object} elt l'élément ciblé
     */
    const hideLevelPreview = function (elt){
        if (elt.nextSibling.nodeName==='IMG')
            elt.parentNode.removeChild(elt.nextSibling);
    };

    /**
     * Supprime le parcours pour un professeur depuis la table t_parcours
     * @param  {string} parcoursId l'id du parcours
     */
    const deleteParcoursWithId = function (parcoursId){
        ui_global.showConfirmAlert('warning','Supprimer ce parcours','Voulez-vous vraiment supprimer ce parcours ?','Oui, je supprime !',function (){
            wrk.send('/deleteParcoursWithId', true, {
                parcoursId:parcoursId
            }, function (data, success) {
                if (success) {
                    // Après avoir supprimé depuis la BD, on supprime depuis l'affichage client pour éviter de relire dans la BD
                    $('#'+parcoursId).remove();
                }
            }, true);
        });
    };

    /**
     * Fonction récursive qui supprime des parcours tant qu'il y a des id dans le tableau
     * @param  {string} idArray tableau des id des parcours à supprimer
     */
    const deleteMultipleParcours = function (idArray){
        const longueurTableau = idArray.length;
        if (longueurTableau > 0)
        wrk.send('/deleteParcoursWithId', true, {
            parcoursId:idArray[longueurTableau-1]
        }, function (data, success) {
            if (success) {
                // Après avoir supprimé depuis la BD, on supprime depuis l'affichage client pour éviter de relire dans la BD
                $('#'+idArray[longueurTableau-1]).remove();
                idArray.pop();
                deleteMultipleParcours(idArray);
            }
        }, true);
    };


        /**
     * Fonction pour la sauvegarde d'un parcours (suite de assignParcours)
     * @param  {string} target type de l'élément cliqué (classe ou élève)
     * @param  {int} targetId l'id de l'élément cliqué (classe ou élève)
     * @param  {string} clickedName nom de l'élément cliqué (classe ou élève)
     */
    const saveParcoursTo = function (target, targetId, clickedName){
        wrk.send('/saveParcoursTo', true, {
            target:target,
            targetId: targetId,
            clickedName:clickedName
        }, function (data, success) {
            if (success) {
                console.log('parcours ajouté à la classe');
            }
        }, true);
    };


    /**
     * Fonction pour la sauvegarde d'un parcours pour une classe ou un élève
     * @param  {string} elt l'élément cliqué
     * @param  {int} targetId l'id de l'élément cliqué (classe ou élève)
     * @param  {string} clickedName nom de l'élément cliqué (classe ou élève)
     * @param  {string} target type de l'élément cliqué (classe ou élève)
     */
    const assignParcours = function (elt, targetId, clickedName, target){
        // S'il est déjà coché : on va supprimer
        if (elt.hasClass('fa-check-square')){
            // Si l'élément cliqué est une classe
            if (target ==='class'){
                // On supprime le parcours pour la classe
                elt.removeClass('fa-check-square');
                elt.addClass('fa-square');
                deleteParcoursFrom(target,targetId,clickedName);
                // Et on supprime le parcours pour tous les élèves de la classe ci-dessus
                const listeEleves = $('#lstEleves a');
                listeEleves.each(function() {
                    const eleveIdBrut = $(this).attr('id');
                    const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
                    deleteParcoursFrom('student',eleveId,$(this).text());
                });
            }
            else{
                // Sinon, on supprime le parcours une seule fois (pour un élève)
                elt.removeClass('fa-check-square');
                elt.addClass('fa-square');
                deleteParcoursFrom(target,targetId,clickedName);
                // Et on le supprime de la calsse de l'élève car il n'est plus commun à toute la classe
                const listeClasses = $('#lstClasses a');
                listeClasses.each(function() {
                    const classIdBrut = $(this).attr( 'id' );
                    const classId = classIdBrut.substring(6, classIdBrut.length);
                    if ($(this).hasClass('selected')){
                        $(this).find('i').removeClass('fa-check-square');
                        $(this).find('i').addClass('fa-square');
                        deleteParcoursFrom('class',classId,$(this).text());
                     }
                }
                );
            }
        }
        // S'il n'est pas coché
        else{
            // Si l'élément cliqué est une classe
            if (target ==='class'){
                // On ajoute le parcours à la classe
                elt.removeClass('fa-square');
                elt.addClass('fa-check-square');
                saveParcoursTo(target,targetId,clickedName);
                // Et on ajoute le parcours à tous les élèves de la classe ci-dessus
                const listeEleves = $('#lstEleves a');
                listeEleves.each(function() {
                    const eleveIdBrut = $(this).attr('id');
                    const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
                    saveParcoursTo('student',eleveId,$(this).text());
                });
            }
            // Sinon, on ajoute le parcours une seule fois (à un élève)
            else{
                elt.removeClass('fa-square');
                elt.addClass('fa-check-square');
                saveParcoursTo(target,targetId,clickedName);
            }
        }
    };

    /**
     * Change la vue en fonction de l'onglet envoyé en paramètre
     */
    const toggleParcoursView = function(elt){
        const id = elt.attr('id');
        switch (id) {
            case 'toggleMetaData':
                $('#stepsGlobal').css({'display':'none'});
                $('#assignParcours').css({'display':'none'});
                $('#globalMetaData').css({'display':'block'});
                break;
            case 'toggleSteps':
                $('#globalMetaData').css({'display':'none'});
                $('#assignParcours').css({'display':'none'});
                $('#stepsGlobal').css({'display':'block'});
                $('.copieNomParcours').text(nomParcours.val());
                break;
            case 'toggleAssignParcours':
                $('#stepsGlobal').css({'display':'none'});
                $('#globalMetaData').css({'display':'none'});
                $('#assignParcours').css({'display':'block'});
                $('.copieNomParcours').text(nomParcours.val());
                loadCheckBoxForClasses();
                break;
            default:
                break;
        }
    };

    /**
     * Charge la liste des parcours pour la classe ou l'élève sélectionné
     */
    const loadParcoursByTarget = function(element){
        const list = $(element+' a');
        // On récupère l'id de l'élément sélectionné
        list.each(function() {
            if ($(this).hasClass('selected')){
                const idBrut = $(this).attr( 'id' );
                let id ;
                if (idBrut.indexOf('classe')>=0){
                    id = idBrut.substring(6, idBrut.length);
                    window.location.href='/parcoursClasse/'+id;
                }
                else{
                    id = idBrut.substring(5, idBrut.length);
                    window.location.href='/parcoursEleve/'+id;
                }
            }
        });
    };

    /**
     * Charge la liste des parcours privés pour le professeur
     * @param  {string} parcoursPrives liste des parcours
     * @param  {source} destinationId on adapte l'affichage selon la destination
     */
    const loadParcoursByUser = function(parcoursPrives,destinationId){
        if (destinationId!=='')
            $('#'+destinationId).empty();
        for(var i = 0; i < parcoursPrives.length; i++) {
            const competences = JSON.parse(parcoursPrives[i].competences);
            $('#listeParcoursPrives').append('<div id="'+parcoursPrives[i].id+'" name="parcoursPrive" date="'+parcoursPrives[i].created+'" class="classe d-flex list-group-item list-group-item-action align-items-center" data-bs-toggle="list">' +
                '<input id="check'+parcoursPrives[i].id+'" name="parcoursPriveCheckBox" type="checkbox" value="" onclick="ui_global.toggleMultipleSelect(`parcoursPriveCheckBox`)">' +
                '<a class="col-lg-4 col-md-4 col-10 ml-lg-3" href="ajouterParcours/'+i+'/'+parcoursPrives[i].id+'/'+parcoursPrives[i].nom+'/'+competences.discipline+'/'+competences.competence+'/'+competences.code+'/'+competences.objectif+'" role="tab">'+parcoursPrives[i].nom+'</a><span class="dateParcours col-3 d-none d-lg-block d-md-block"><strong>Créé : </strong>'+parcoursPrives[i].created+'</span><span class="dateParcours col-3 d-none d-lg-block d-md-block"><strong>Modifié : </strong>'+parcoursPrives[i].updated+'</span><span class="deleteAndShare"><i class="corbeille" data-feather="trash-2" onclick="ui_parcours.deleteParcoursWithId('+parcoursPrives[i].id+')"></i><a title="partager" onclick="ui_parcours.updateParcoursStatus('+parcoursPrives[i].id+',1,'+i+')"><i class="sharing" data-feather="share-2" ></i></a></span></div>');
        }
        feather.replace();
    };

    /**
     * Fonction pour rajouter un parcours graphiquement dans une liste (en cas de changement de statut) afin d'éviter de recharger la liste des parcours en interrogeant la base de données à chaque fois.
     * Cette fonction est appelée à partir de trois sources différentes, donc des adaptations sont faites en fonction de chaque appel
     * @param  {string} parcours Reçoit l'objet parcours
     * @param  {string} destination id de la liste de destination du parcours
     * @param  {string} button le bouton changera en fonction du statut du parcours (public, privé, en attente)
     */
    const appendParcoursToList = function (parcours,destination,button){
        if (destination===''){
            // Si on est sur la page d'accueil, pas besoin d'aœller plus loin
            return;
        }
        if (destination!=='listeParcoursPrives')
            $('#'+parcours.id).hide();
        $('#'+destination).append('<div id="'+parcours.id+'" class="classe list-group-item list-group-item-action col-12"><a href="ajouterParcours/997/'+parcours.id+'/'+parcours.competences.discipline+'/'+parcours.competences.competence+'/'+parcours.competences.code+'/'+parcours.competences.objectif+'">'+parcours.nom+'</a><span class="dateParcours float-right"><strong>Licence : </strong>'+parcours.licence+'<strong class="ml-5">Créé : </strong>'+parcours.created+'<strong class="ml-5">Propriétaire : </strong>'+parcours.owner+button+'<i class="corbeille" data-feather="trash-2" onclick="ui_parcours.deleteParcoursWithId('+parcours.id+')"></i></span></div>');
        feather.replace();
    };

    /**
     * Requête async. vers le système LRS du LIRIS
     */
     const sendDatatoLRS = function(){
         const json ={ id:1, name:'activite'};
         wrk.send('/sendDatatoLRS', true, {
             exoId : json.id,
             exoName: json.name
         }, function (data, success) {
             if (success) {
                console.log('success');
                    }
             else
                 console.log('fail');
             }, true);
     };

    /**
     * Modifie le statut d'un parcours (privé / en attente / public)
     * Cette fonction est appelée à partir de trois sources différentes, donc des adaptations sont faites en fonction de chaque appel
     * @param {String} element soit l'objet parcours, soit son id uniquement
     * @param {int} status la valeur du statut : 0=privé, 1 = en attente de publication, 2 = public
     * @param {String} parcoursNumber numéro du parcours dans la liste PrivateParcours (à changer)
     */
    const updateParcoursStatus = function(element, status,parcoursNumber){
        //parcoursPrives, pendingParcours,
        // On cherche la source du click pour faire les adaptations nécessaires
        let source;
        let parcoursId;
        if (element.constructor === {}.constructor){
            source = $('#'+element.id).parent().attr('id');
            parcoursId = element.id;
        }
        else{
            source = $('#'+element).parent().attr('id');
            parcoursId = element;
        }
        let title;
        let text;
        let destination='';
        let button;
        if(status===PARCOURS_PENDING){ // Le parcours passe en attente de validation
            if (source==='listeParcoursPrives'){// correspond à la page classes.ejs
                title='Rendre public ce parcours';
                text='Votre demande doit être validée par un Admin';
                // destination, button restent nuls car pas d'ajout graphique coté client
            }
            else if(source==='parcoursValides'){ // correspond à la page validerParcours.ejs
                title='Remettre en attente de validation';
                text='Ce parcours ne sera plus visible pour tout le monde';
                destination='pendingParcours';
                button='<button class="ml-3 btn btn-outline-success" onclick="alert(`nous avons un petit souci. Veuillez rafraichir la page`);">Valider</button>';
            }
        }
        else if(status===PARCOURS_PUBLIC){
            title='Rendre public ce parcours';
            text='Tout le monde pourra utiliser ce parcours';
            destination='parcoursValides';
            button='<button class="ml-3 btn btn-outline-warning" onclick="alert(`nous avons un petit souci. Veuillez rafraichir la page`);">Remettre en attente</button>';
        }
        ui_global.showConfirmAlert('question',title,text,'Confirmer',function (){
            wrk.send('/updateParcoursStatus', true, {
                id : parcoursId,
                status : status,
                parcoursNumber : parcoursNumber
            }, function (data, success) {
                if (success) {
                    location.reload();
                }
                else{
                    ui_global.showAlert('info','Impossible de partager car vous avez des activités privées dans ce parcours');
                }
            }, true);
        });
    };

    /**
     * Charge un parcours depuis la BDD
     * @param savedStepsJson le json global de toutes les étapes du parcours
     * @param jeux la liste des jeux (utile pour avoir les images des jeux sur chaque étape)
     */
    const loadParcourSteps = function(savedStepsJson,jeux){
        globalStepsArray = [];
        if (jeux)
            gameList = jeux;
        if (savedStepsJson){
            const parsedSteps = JSON.parse(savedStepsJson);
            for(var i = 0; i < parsedSteps.length; i++) {
                var step = parsedSteps[i];
                if (step.type===_const.normalStep){
                    addNewStep(step.niveau,step.fullName,step.type,step.gameId,step.id,getStepImage(step.gameId,gameList));
                }
                else if (step.type===_const.remediationStep){
                    addRemediation(step.defaultRemediationId,step.niveau,step.fullName,step.gameId,step.id,getStepImage(step.gameId,gameList));
                }
            }
        }
    };

    /**
     * Récupère l'image d'un niveau/étape à partir de la liste des jeux
     * @param stepId l'id du niveau
     * @param jeux la liste des jeux
     */
    const getStepImage = function(stepId, jeux){
        for (const jeu of jeux){
            if (stepId == jeu.id)
                return jeu.image;
        }
        return null;
    };

    /**
     * Charge les checkbox pour les classes
     */
    const loadCheckBoxForClasses = function(){
        const listeClasses = $('#lstClasses a');
        // On parcours la liste des classes affichée
        listeClasses.each(function() {
            const classIdBrut = $(this).attr( 'id' );
            const classId = classIdBrut.substring(6, classIdBrut.length);
            wrk.send('/checkParcoursFor', true, {
                target:'class',
                targetId:classId
            }, function (data, success) {
                // Si on trouve le parcours dans la base on coche la case
                if (success) {
                    $('#'+classIdBrut).find('i').removeClass('fa-square');
                    $('#'+classIdBrut).find('i').addClass('fa-check-square');
                }
            }, true);
        });
    };

    /**
     * Charge les checkbox pour les étudiants
     */
    const loadCheckBoxForStudents = function(){
        const listeEleves = $('#lstEleves a');
        // On parcours la liste des élèves affichée
        listeEleves.each(function() {
            const eleveIdBrut = $(this).attr('id');
            const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
            wrk.send('/checkParcoursFor', true, {
                target:'student',
                targetId:eleveId
            }, function (data, success) {
                // Si on trouve le parcours dans la base on coche la case
                if (success) {
                    $('#'+eleveIdBrut).find('i').removeClass('fa-square');
                    $('#'+eleveIdBrut).find('i').addClass('fa-check-square');
                }
            }, true);
        });
    };

    /**
     * Ajoute un nouveau parcours (page métadonnées)
     */
    const addParcours = function(param){
        wrk.send('/addParcours', true, {
            msg:"ok"
        }, function (data, success) {
            if (success) {
                alert(param);
                console.log('ok');
            }
        }, true);
    };

    /**
     * Prévisualise l'image après upload dans la page validation exercice
     */
    const previewImageValidation = function(){
            const file = document.querySelector('input[type=file]').files[0];
            const reader = new FileReader();
            reader.addEventListener('load', function () {
                console.log(reader.result);
                $('#img_preview').attr('src', reader.result);
            }, false);
            if (file) {
                reader.readAsDataURL(file);
            }
        };

    /**
     * Insère une donnée dans l'historique des exercices
     * @param {string} stepId l'id du step (=bouton à mettre à jour avec l'id de la statistique à utiliser lors de la validation)
     */
    const createStatsEntry = function (mode,gameId,stepId){
        wrk.send('/addStats', false, {
            mode : mode,
            gameId : gameId,
            forcedExerciceId : stepId
        }, function (data, success) {
            console.log(data.data);
            document.getElementById('btn-'+stepId).addEventListener('click',function (){
                ui_parcours.updateStatsEntry(data.data.id,2);
            });
            }, true);
    };

    /**
     * Met à jour l'historique avec l'évaluation obtenue
     * @param {string} statId l'id de la statistique à mettre à jour
     */
    const updateStatsEntry = function (statId,evaluation){
        wrk.send('/updateStats', false, {
            id:statId,
            evaluation : evaluation,
        }, function (data, success) {
            location.reload();
            }, true);
    };

    /**
     * Met à jour l'id du step graphiquement et dans le json
     * @param {int} stepNumber le numéro du step dans le json (à remplacer par le dataBaseId)
     * @param {int} dataBaseId l'id créé suite à l'insertion dans la base de données
     */
    const updateGraphicsAndJson = function (stepNumber,dataBaseId){
        // update graphique DOM
        if (!document.getElementById(dataBaseId)) {
            // Update de l'id du step
            document.getElementById(stepNumber).setAttribute(_const.HTMLId, dataBaseId);
            // Update de l'id du inputConsigne
            document.getElementById('inputConsigne'+stepNumber).setAttribute(_const.HTMLId, 'inputConsigne'+dataBaseId);
            // Update de l'id du step (on récupère plutot le step.consigne finalement
            // document.getElementById('editStepJson'+stepNumber).setAttribute('onclick', 'ui_parcours.saveUnpluggedContent('+dataBaseId+')');
        }
        // update dans le json
        let defaultRemediationId = '';
        if (globalStepsArray[stepNumber-1].type===_const.remediationStep)
            defaultRemediationId = getParentStepNumber(globalStepsArray[stepNumber].defaultRemediationId);
        const newStepJson = createStepJson(defaultRemediationId,stepNumber,'Debra','debranch','debra',globalStepsArray[stepNumber-1].type,dataBaseId);
        globalStepsArray[stepNumber-1] = newStepJson;
    };

    /**
     * Met à jour le step dans la base de données
     * @param {int} stepId l'id du step
     * @param {string} content le contenu à assigner au step
     */
    const updateUnpluggedLevelContent = function (stepId,content){
        // ajouter la maj du json
        wrk.send('/updateUnpluggedExercice', false, {
            stepId:stepId,
            content: content
        }, function (data, success) {
            console.log('success : ' + data.data);
        }, true);
    };

    /**
     * Prévisualise l'image après upload dans la page validation exercice
     */
    const checkPasswordForId = function(){
        Swal.fire({
            title: 'Supprimer ce parcours',
            text: 'Voulez-vous vraiment supprimer ce parcours ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, je supprime !',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.isConfirmed) {
                wrk.send('/deleteParcoursWithId', true, {
                    parcoursId:999
                }, function (data, success) {
                    if (success) {
                        // Après avoir supprimé depuis la BD, on supprime depuis l'affichage client pour éviter de relire dans la BD
                        console.log('ok');
                    }
                }, true);
            }
            else
                alert('not confirmed');
        });
    };

    /**
     * Modifie le json d'une étape pour la valider manuellement
     * @param {Object} stepsJson le json de l'étape à mettre à jour
     * @param {int} stepNumber le numéro de l'étape
     * @param {int} evaluation détermine si l'étape a été validée ou pas
     */
    const editStepStatusInJson = function(stepsJson, stepNumber, evaluation){
        if (stepsJson!==null){
            if (evaluation){
                stepsJson[stepNumber].done = evaluation;
                // console.log(stepsJson);
                wrk.send('/updateStepsJson', true, {
                    stepsJson:stepsJson
                }, function (data, success) {
                    if (success) {
                        location.reload();
                    }
                }, true);
            }
        }
    };

    /**
     * @param {Object} stepNode le noeud HTML créé pour l'étape
     * @param {int} stepId l'id de l'étape
     * @param {string} gameName Le nom du jeu concernant l'étape
     * @param {string} stepLevel Le niveau de l'étape
     */
    const fillStepInformation = function(stepNode,stepId, gameName, stepLevel,gameImage, gameId){
        const gameLabelDiv = stepNode.children[0].children[1].children[0];
        const stepLabelDiv = stepNode.children[0].children[1].children[1];
        const gameImageDiv = stepNode.children[0].children[0];
        stepNode.setAttribute('id',stepId);
        stepNode.setAttribute('number',nb_steps);
        if (gameImage)
            gameImageDiv.setAttribute('src',gameImage);
        gameLabelDiv.textContent = gameName;
        stepLabelDiv.textContent = stepLevel;

        if (gameId===_const.unpluggedActivityId||gameId===_const.externalLinkId)
            stepLabelDiv.setAttribute('data-target','#unplugged'+stepId);
        else
            stepLabelDiv.setAttribute('data-target','#id-'+stepId);
        return stepNode;
    };

    /**
     * Génère les modals pour les activités débranchées et externes
     */
    const createUnpluggedModals = function(){
        let unpluggedModal = ui_global.createHTMLnode(HTMLNodes.basicModal.HTMLNode,HTMLNodes.basicModal.attributes,null,HTMLNodes.basicModal.children);
        let externalModal = ui_global.createHTMLnode(HTMLNodes.basicModal.HTMLNode,HTMLNodes.basicModal.attributes,null,HTMLNodes.basicModal.children);
        unpluggedModal = setupModal(unpluggedModal,_const.unpluggedActivityId,'/media/controls/teacher.png','Créer une nouvelle activité débranchée',HTMLNodes.unpluggedModalContent,'consigne');
        externalModal = setupModal(externalModal,_const.externalLinkId,'/media/controls/browser.png','Créer une nouvelle activité externe',HTMLNodes.unpluggedModalContent,'URL');
        document.body.appendChild(unpluggedModal);
        document.body.appendChild(externalModal);
    };

    /**
     * Personnalise le modal
     * @param {boolean} editMode vrai si on va éditer le modal, false sinon
     * @param {HTMLObjectElement} modalNode le noeud HTML
     * @param {string} modalId l'id du modal à générer
     * @param {string} headerImage l'image à apparaître sur le header du modal
     * @param {string} headerTitle le titre à apparaître sur le header du modal
     * @param {HTMLObjectElement} modalBodyNode le noeud du corps du modal
     * @param {string} inputLabel consigne ou URL
     */
    const setupModal = function(modalNode,modalId,headerImage,headerTitle,modalBodyNode,inputLabel){
        modalNode.setAttribute('id','unplugged'+modalId);
        modalNode = setupModalHeader(modalNode,headerImage,headerTitle);
        modalNode = setupModalBody(modalNode,modalBodyNode,inputLabel);
        modalNode = setupModalFooter(modalNode,modalId,headerImage,inputLabel);
        return modalNode;
    };

    /**
     * Personnalise le header du modal
     */
    const setupModalHeader = function(modalNode,headerImage,headerTitle){
        const headerImageDiv = modalNode.children[0].children[0].children[0].children[0];
        const headerTitleDiv = modalNode.children[0].children[0].children[0].children[1];
        const headerCloseButtonDiv = modalNode.children[0].children[0].children[0].children[2];
        headerImageDiv.setAttribute('src',headerImage);
        headerTitleDiv.textContent = headerTitle;
        headerCloseButtonDiv.textContent = 'Ⓧ';
        return modalNode;
    };

    /**
     * Personnalise le contenu du modal
     */
    const setupModalBody = function(modalNode,modalBodyNode,inputLabel){
        const modalBody = modalNode.children[0].children[0].children[1];
            modalBody.appendChild(ui_global.createHTMLnode(modalBodyNode.HTMLNode,modalBodyNode.attributes,null,modalBodyNode.children));
            modalBody.children[0].children[0].children[0].textContent= 'Nom de l\'activité';
            modalBody.children[0].children[0].children[1].setAttribute('placeholder','Insérer le nom de votre activité');
            modalBody.children[0].children[1].children[0].textContent= inputLabel;
            modalBody.children[0].children[1].children[1].setAttribute('placeholder','Insérer ici votre '+inputLabel);
            modalBody.children[0].children[1].children[1].setAttribute('role',inputLabel);
            modalBody.children[0].children[2].children[0].textContent= 'Description';
            modalBody.children[0].children[2].children[1].setAttribute('placeholder','Insérer une description (facultatif)');
        return modalNode;
    };

    /**
     * Personnalise le footer du modal
     */
    const setupModalFooter = function(modalNode,modalId,gameImage,inputLabel){
        const modalfooter = modalNode.children[0].children[0].children[2];
        const modalForm = modalNode.children[0].children[0].children[1].children[0];
        modalfooter.children[0].textContent = 'Annuler';
        modalfooter.children[1].textContent= 'Créer l\'activité';
        modalfooter.children[1].addEventListener('click',function () {
            ui_parcours.createUnpluggedActivity(modalForm,modalId,gameImage,inputLabel);
        });
        return modalNode;
    };

    /**
     * Créer une nouvelle activité débranchée / externe dans la base de données
     */
    const createUnpluggedActivity = function(modalForm,modalId,gameImage,inputLabel){
        const formJson = ui_global.checkModalFields(modalForm,gameImage,inputLabel);
        if (formJson.name && formJson.consigne)
            wrk.send('/createUnpluggedActivity', true, {
                exercice : JSON.stringify(formJson),
                gameId : modalId,
                exoName : formJson.name
            }, function (data, success) {
                if (success) {
                    location.reload();
                }
            }, true);
    };

    /**
     * Supprime un exercice depuis la BD (peut être utilise pour les unplugged)
     */
    const deleteExercice = function(exoId){
        if (confirm('Voulez-vous vraiment supprimer cette activité ?')){
            wrk.send('/deleteExercice', true, {
                exoId
            }, function (data, success) {
                if (success) {
                    // Après avoir supprimé depuis la BD, on supprime graphiquement pour éviter de relire dans la BD
                    document.querySelector('a[stepId="'+exoId+'"]').remove();
                }
            }, true);
        }
    };

    /**
     * Réinitialise un parcours pour un élève en supprimant les historiques de ses exercices dans le parcours
     */
    const resetParcoursForStudent = function(){
        if (confirm('Voulez-vous vraiment ré-initialiser ce parcours ?')){
            wrk.send('/resetParcoursForStudent', true,{}, function (data, success) {
                if (success) {
                    location.reload();
                }
            }, true);
        }
    };

    return {

    afficherCompetences,
    afficherObjectifs,
    afficherAttente,
    appendParcoursToList,
    showTagInput,
    addParcours,
    assignParcours,
    addNewStep,
    addRemediation,
    createStatsEntry,
    changeText,
    checkStep,
    checkPasswordForId,
    createStep,
    createStepJson,
    createUnpluggedActivity,
    createUnpluggedModals,
    duplicatePublicParcours,
    deleteExercice,
    deleteParcoursFrom,
    deleteParcoursWithId,
    deleteMultipleParcours,
    editStepStatusInJson,
    fillStepInformation,
    getParentStepNumber,
    hideLevelPreview,
    loadCheckBoxForClasses,
    loadCheckBoxForStudents,
    loadParcoursByTarget,
    loadParcoursByUser,
    loadParcourSteps,
    makeItDroppable,
    moveStep,
    normalizeString,
    previewImageValidation,
    removeStep,
    removeStepFromGlobalStepsArray,
    resetParcoursForStudent,
    saveParcours,
    saveParcoursTo,
    sendDatatoLRS,
    setParcours,
    setupModal,
    setupModalBody,
    setupModalFooter,
    setupModalHeader,
    showLevelPreview,
    toggleParcoursPER,
    toggleParcoursView,
    unSetParcours,
    updateGraphicsAndJson,
    updateParcoursStatus,
    updateStatsEntry,
    updateUnpluggedLevelContent,
    updateOverflow
};

})();
