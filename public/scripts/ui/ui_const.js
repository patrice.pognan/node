/**

 * @classdesc Fichier pour stocker les valeures de variables utilisées par les classes ui_..js

 * @author Aous Karoui

 * @version 1.0

 */

const konst = {

    HTMLDiv : 'div'
};

module.exports =  konst;