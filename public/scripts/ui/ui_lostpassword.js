/**
 * @classdesc Contrôleur de la page de répurération de mot de passe
 * @author Florian Kolly
 * @version 1.0
 */
var ui_lostpassword = (function () {

  $(document).ready(function () {

    const input_email = $('#email');
    const input_code = $('#code');
    const btn_email = $('#btn_email');
    const btn_code = $('#btn_code');

    input_code.on('change input', function () {
      input_code.val().length === 8 ? btn_code.attr('disabled', false) : btn_code.attr('disabled', true);
    });

    input_email.on('input change', function () {
      const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      re.test(input_email.val()) ? btn_email.attr('disabled', false) : btn_email.attr('disabled', true);
    });
  });


  const sendMail = function () {
    let email = $('input[name=email]').val();
    wrk.send('/sendVerificationmail', true, { email: email }, function (data, success) {
    });
  }

  const sendCode = function () {
    let code = $('input[name=code]').val();
    let email = $('input[name=email]').val();
    wrk.send('/lostpassword', false, { code: code, email: email }, function (data, success) {
      if (success) {
        window.location.pathname = '/modifyPassword';
        $('input[name=code').val('');
      }
    });
  }

  return {
    sendMail: sendMail,
    sendCode: sendCode
  }

})();

