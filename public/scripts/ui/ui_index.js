/**
 * @classdesc Affiche le splashscreen et le fait disparaître au bout de 4 secondes
 * @author Florian Kolly
 * @version 1.0
 */
$(document).ready(function() {
  if (!sessionStorage.getItem('splash')) {
    $('#splash-overlay').show();
    setTimeout(function(){
      $('#splash-overlay').fadeOut('slow');
    }, 4000);
    sessionStorage.setItem('splash', true);
  }
});
