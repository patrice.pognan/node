/**

 * @classdesc Contrôleur de la page de validation

 * @author Sturzenegger Erwan

 * @version 1.0

 */

var ui_modifier_exercice_5 = (function () {

    var currentId = -1;

    var allShapes = [];
    var trueShapes = [];

    var buttons = {
        validated: [],
        selected: -1
    };
    const levelInput = $('#niveauExo');
    const textArea = $('#descriptionExo');
    var currentQuestion = {};

    const chooseQuestion = function (id) {
        buttons.selected = id;
        currentId = id;
        currentQuestion = exercice.questions[id];

        labelFR = $('#labelFr');
        labelDE = $('#labelDe');
        labelEN = $('#labelEn');
        labelIT = $('#labelIt');

        labelFR.val(currentQuestion.label.fr);
        labelDE.val(currentQuestion.label.de);
        labelEN.val(currentQuestion.label.en);
        labelIT.val(currentQuestion.label.it);

        $('#typeDraw').prop('checked', currentQuestion.type === 'draw');
        $('#typeCheckbox').prop('checked', currentQuestion.type === 'qcm' && currentQuestion.qcmtype === 'checkbox');
        $('#typeRadio').prop('checked', currentQuestion.type === 'qcm' && currentQuestion.qcmtype === 'radio');

        if (currentQuestion.type === 'draw')
            $('#trueShapesContainer').hide();
        else
            $('#trueShapesContainer').show();

        updateLabel();
        allShapes = [];
        trueShapes = [];
        for (let i = 0; i < shapes.length; i++) {
            $('#allShapes_' + i).prop('checked', false);
            $('#trueShapes_' + i).hide();
            $('#trueShapesBox_' + i).prop('checked', false);
        }
        for (let i = 0; i < currentQuestion.responses.length; i++) {
            updateShapes(currentQuestion.responses[i]);
        }

        updateButtons();

    };

    const updateLabel = function () {
        var lbl = labelFR.val();
        if (lbl.length > 75) {
            labelFR.addClass('is-invalid');
            labelFR.removeClass('is-valid');
        } else {
            labelFR.addClass('is-valid');
            labelFR.removeClass('is-invalid');
        }
        var lbl = labelDE.val();
        if (lbl.length > 75) {
            labelDE.addClass('is-invalid');
            labelDE.removeClass('is-valid');
        } else {
            labelDE.addClass('is-valid');
            labelDE.removeClass('is-invalid');
        }
        var lbl = labelEN.val();
        if (lbl.length > 75) {
            labelEN.addClass('is-invalid');
            labelEN.removeClass('is-valid');
        } else {
            labelEN.addClass('is-valid');
            labelEN.removeClass('is-invalid');
        }
        var lbl = labelIT.val();
        if (lbl.length > 75) {
            labelIT.addClass('is-invalid');
            labelIT.removeClass('is-valid');
        } else {
            labelIT.addClass('is-valid');
            labelIT.removeClass('is-invalid');
        }
    };

    const selectTrueShape = function (id) {
        var type = $('input[name=type]:checked').val();
        if (type === 'checkbox') {
            if (trueShapes.indexOf(id) >= 0)
                trueShapes.splice(trueShapes.indexOf(id), 1);
            else
                trueShapes.push(id);
        } else if (type === 'radio') {
            $('#trueShapesBox_' + trueShapes[0]).prop('checked', false);
            trueShapes = [id];
        }
    };

    const updateTrueShapes = function (id) {
        if (allShapes.indexOf(id) >= 0) {
            allShapes.splice(allShapes.indexOf(id), 1);
            $('#trueShapes_' + id).hide();
        } else {
            $('#trueShapes_' + id).show();
            allShapes.push(id);
        }
    };

    const askForRestore = function () {
        chooseQuestion(currentId);
    };

    const askForSave = function (callback = null) {
        exercice.questions[currentId].label.fr = $('#labelFr').val();
        exercice.questions[currentId].label.de = $('#labelDe').val();
        exercice.questions[currentId].label.en = $('#labelEn').val();
        exercice.questions[currentId].label.it = $('#labelIt').val();

        var questionShapes = [];

        var type = $('input[name=type]:checked').val();

        for (var shapeId of allShapes) {
            questionShapes.push({
                value: shapes[shapeId].id,
                response: (type === 'draw' ? true : (trueShapes.indexOf(shapeId) >= 0))
            });
        }

        exercice.questions[currentId].responses = questionShapes;

        switch (type) {
            case 'radio':
                exercice.questions[currentId].type = 'qcm';
                exercice.questions[currentId].qcmtype = 'radio';
                break;
            case 'checkbox':
                exercice.questions[currentId].type = 'qcm';
                exercice.questions[currentId].qcmtype = 'checkbox';
                break;
            case 'draw':
                exercice.questions[currentId].type = 'draw';
                exercice.questions[currentId].qcmtype = null;
                break;
        }

        // L'indication du niveau de l'exercice est obligatoire
        if (levelInput.val()===''){
            levelInput.css('border-color','red');
            levelInput.focus();
        }
        else{
            wrk.send('/updateExercice', false, {exercice: JSON.stringify(exercice), levelInput:levelInput.val(), exId: exId}, function (data, success) {
                if (success) {
                    ui_global.showToast('success', data.message);
                    currentId++;
                    if (currentId >= exercice.questions.length - 1) {
                        if (currentId >= exercice.questions.length)
                            currentId--;
                        $('#btnSauvegarder').hide();
                        $('#btnValider').show();
                    }
                } else
                    ui_global.showToast('error', data.message);
                buttons.validated.push(currentId - 1);
                chooseQuestion(currentId);
                if (callback) {
                    callback();
                }
            }, true);
        }

    };

    const updateShapes = function (response) {
        var index = 0;
        for (let shape of shapes) {
            if (shape.id === response.value)
                break;
            index++;
        }
        $('#trueShapes_' + index).show();
        $('#allShapes_' + index).prop('checked', true);
        if (response.response) {
            $('#trueShapesBox_' + index).prop('checked', true);
            trueShapes.push(index);
        }
        allShapes.push(index);
    };

    const validate = function () {
        const levelInput = $('#niveauExo');
        const textArea = $('#descriptionExo');
        const imageB64 = $('#img_preview').attr('src');

        askForSave(function () {
            wrk.send('/updateExerciceState', false, {description:textArea.val(), state: 2, levelInput:levelInput.val(), imageB64, exId: exId}, function (data, success) {
                Swal.fire({
                    title: data.message,
                    icon: data.status,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    $('#backBtn').click();
                });
            }, true);
        });

    };

    const updateButtons = function () {
        $('.question-btn').removeClass('selected');

        for (id of buttons.validated)
            $('#questionActive' + id).addClass('validated');


        $('#questionActive' + buttons.selected).addClass('selected');
        $('#questionActive' + buttons.selected).removeClass('validated');

        $('#questionActive' + buttons.selected).show();
        $('#questionInactive' + buttons.selected).hide();
    };

    const changeMode = function () {
        var type = $('input[name=type]:checked').val();
        if (type === 'draw')
            $('#trueShapesContainer').hide();
        else
            $('#trueShapesContainer').show();
        allShapes = [];
        trueShapes = [];
        for (var i = 0; i < shapes.length; i++) {
            $('#trueShapesBox_' + i).prop('checked', false);
            $('#allShapes_' + i).prop('checked', false);
            $('#trueShapes_' + i).hide();
        }
    };

    return {
        chooseQuestion,
        updateLabel,
        selectTrueShape,
        updateTrueShapes,
        askForRestore,
        askForSave,
        validate,
        changeMode
    };
})();

