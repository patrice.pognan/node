/**

 * @classdesc Contrôleur de la page de validation

 * @author Sturzenegger Erwan

 * @version 1.0

 */

var ui_modifier_exercice_1 = (function () {

    var sprites = {};

    const buildImage = function () {
        wrk.send('/getSprites', false, {gameId: 1}, function (data, success) {
            fillSprites(data);
            var image = buildLevel(0);
        }, true);
    };

    const buildLevel = function (levelId) {
        var ctrl = document.getElementById('canvas').getContext('2d');
        ctrl.clearRect(0, 0, 500, 500);
        var level = exercice.levels[levelId];
        var cells = [];
        var bgSize = 500 / level.size;

        var bgImg = new Image();
        bgImg.onload = function () {

            for (var i = 0; i < level.size; i++) {
                for (var j = 0; j < level.size; j++) {
                    ctrl.drawImage(bgImg, j * bgSize, i * bgSize, bgSize, bgSize);
                }
            }
        };
        bgImg.src = sprites.cell;

        var batImg = new Image();
        batImg.onload = function () {
            for (var bat of level.cells.walls) {
                ctrl.drawImage(batImg, 0, 0, 32, 32, bat.x * bgSize + (bgSize - 32 * 1.1) / 2, bat.y * bgSize + (bgSize - 32 * 1.1) / 2, 32 * 1.1, 32 * 1.1);
            }
        };
        batImg.src = sprites.bat;

        var coinImg = new Image();
        coinImg.onload = function () {
            for (var coin of level.cells.mandatories) {
                ctrl.drawImage(coinImg, 0, 0, 700 / 6, 200, coin.x * bgSize + (bgSize - ((700 / 6) / 200) * bgSize * 0.8) / 2, coin.y * bgSize + (bgSize - bgSize * 0.8) / 2, ((700 / 6) / 200) * bgSize * 0.8, bgSize * 0.8);
            }
        };
        coinImg.src = sprites.coin;

        var chestImg = new Image();
        chestImg.onload = function () {
            ctrl.drawImage(chestImg, level.cells.stop.x * bgSize + (bgSize - bgSize * 0.8) / 2, level.cells.stop.y * bgSize + (bgSize - bgSize * 0.8) / 2, bgSize * 0.8, bgSize * 0.8);
        };
        chestImg.src = sprites.chest;

        var heroImg = new Image();
        heroImg.onload = function () {
            ctrl.drawImage(heroImg, 0, 0, 512 / 8, 33, level.cells.start.x * bgSize + (bgSize - ((512 / 8) / 33) * bgSize * 0.8) / 2, level.cells.start.y * bgSize + (bgSize - bgSize * 0.8) / 2, ((512 / 8) / 33) * bgSize * 0.8, bgSize * 0.8);
        };
        heroImg.src = sprites.hero;

    };

    const _delete = function (id) {
        wrk.send('/admin/valider/supprimer', false, {exercice: id}, function (response, success) {
            $('#returnBtn').click();
        }, true);
    };

    function fillSprites(data) {
        data = JSON.parse(data);
        for (var sprite of data) {
            var src = 'data:image/png;base64,' + sprite.base64;
            sprites[sprite.nom] = src;
        }
    }

    const saveAndExit = function () {
        const levelInput = $('#niveauExo');
        const textArea = $('#descriptionExo');
        const imageB64 = $('#img_preview').attr('src');
        // L'indication du niveau de l'exercice est obligatoire
        if (levelInput.val()===''){
            levelInput.css('border-color','red');
            levelInput.focus();
        }
        else{
            wrk.send('/updateExerciceState', false, {description:textArea.val(),state: 2, levelInput:levelInput.val(), imageB64, exId: exId}, function (data, success) {
                Swal.fire({
                    title: data.message,
                    icon: data.status,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    $('#backBtn').click();
                });
            }, true);
        }
    };
    return {
        buildImage,
        delete: _delete,
        buildLevel,
        saveAndExit
    };
})();

