/**

 * @classdesc Contrôleur de la page de validation

 * @author Sturzenegger Erwan

 * @version 1.0

 */

var ui_modifier_exercice_4 = (function () {

    var currentChar = {};

    var sprites = {};

    var hasGreatGrandParents = false;

    var infoIntruderSize = 0;

    const buildTree = function () {

        wrk.send('/getSprites', false, {gameId: 4}, function (data, success) {
            fillSprites(data);

            hasGreatGrandParents = !!exercice.scenario.enfant.father.father.mother.birthdate;

            var html = '<div class="tree">' +
                '            <ul>' +
                '                <li>' +
                '                    <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant\')">' + buildInfoDiv(exercice.scenario.enfant) + '</a>' +
                '                    <ul>' +
                '                        <li>' +
                '                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father\')">' + buildInfoDiv(exercice.scenario.enfant.father) + '</a>' +
                '                            <ul>' +
                '                                <li>' +
                '                                    <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.father\',' + !hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.father) + '</a>' +
                (hasGreatGrandParents ?
                    '                                    <ul>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.father.father\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.father.father) + '</a>' +
                    '                                        </li>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.father.mother\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.father.mother) + '</a>' +
                    '                                        </li>' +
                    '                                    </ul>'
                    : '') +
                '                                </li>' +
                '                                <li>' +
                '                                    <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.mother\',' + !hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.mother) + '</a>' +
                (hasGreatGrandParents ?
                    '                                    <ul>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.mother.father\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.mother.father) + '</a>' +
                    '                                        </li>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.father.mother.mother\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.father.mother.mother) + '</a>' +
                    '                                        </li>' +
                    '                                    </ul>'
                    : '') +
                '                                </li>' +
                '                            </ul>' +
                '                        </li>' +
                '                        <li>' +
                '                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother\')">' + buildInfoDiv(exercice.scenario.enfant.mother) + '</a>' +
                '                            <ul>' +
                '                                <li>' +
                '                                    <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.father\',' + !hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.father) + '</a>' +
                (hasGreatGrandParents ?
                    '                                    <ul>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.father.father\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.father.father) + '</a>' +
                    '                                        </li>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.father.mother\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.father.mother) + '</a>' +
                    '                                        </li>' +
                    '                                    </ul>'
                    : '') +
                '                                </li>' +
                '                                <li>' +
                '                                    <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.mother\',' + !hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.mother) + '</a>' +
                (hasGreatGrandParents ?
                    '                                    <ul>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.mother.father\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.mother.father) + '</a>' +
                    '                                        </li>' +
                    '                                        <li>' +
                    '                                            <a href="javascript:ui_modifier_exercice_4.displayInfos(\'enfant.mother.mother.mother\',' + hasGreatGrandParents + ')">' + buildInfoDiv(exercice.scenario.enfant.mother.mother.mother) + '</a>' +
                    '                                        </li>' +
                    '                                    </ul>'
                    : '') +
                '                                </li>' +
                '                            </ul>' +
                '                        </li>' +
                '                    </ul>' +
                '                </li>' +
                '            </ul>' +
                '        </div>';
            document.getElementById('tree').innerHTML = html;


            infoIntruderSize = 90 / exercice.scenario.intrus.length;
            var intruderIndex = 0;
            var intruderHtml = '<div class="intruders"><ul><li><a style="font-size: 20pt; font-weight:bold;">'+trad.intruders+'</a><ul>';
            for (var intruder of exercice.scenario.intrus) {
                intruderHtml += '<li>' +
                    '<a href="javascript:ui_modifier_exercice_4.displayInfos(\'' + intruderIndex + '\',true,true)">' + buildInfoDiv(intruder, true) + '</a>' +
                    '</li>';
                intruderIndex++;
            }
            intruderHtml += '</ul></li></ul></div>';
            document.getElementById('intruders').innerHTML = intruderHtml;
        }, false);
    };

    function buildInfoDiv(infos, isIntruder = false) {
        var html = '<div class="row" style="max-width: ' + (isIntruder ? infoIntruderSize : (hasGreatGrandParents ? 12 : 24)) + 'vw;">' +
            '<div class="col-2 align-self-center">' +
            sprites[infos.image] +
            '</div><div class="col-10" style="text-align: left; overflow-wrap: normal;">';
        html += '<p style="white-space: normal; font-weight: bold;">' + infos.lastName + ' ' + infos.firstName + '</p>';
        html += '<p style="white-space: normal;">' + infos.birthdate + (infos.deathdate ? ' - ' + infos.deathdate : '') + '</p>';
        html += '<p style="white-space: normal;">' + infos.address + '</p>';
        html += '</div></div>';
        return html;
    }

    const displayInfos = function (character, displayParents, isIntruder = false) {
        var path = character.split('.');
        var child = {};
        var foundedChar = exercice.scenario;
        if (isIntruder) {
            foundedChar = exercice.scenario.intrus[parseInt(character)];
        } else {
            for (var i = 0; i < path.length; i++) {
                foundedChar = foundedChar[path[i]];
                if (i < path.length - 1) {
                    child = foundedChar;
                }
            }
        }
        currentChar = foundedChar;

        Swal.fire({
            title: trad.modifyCharacterTitle,
            width: 1000,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: trad.cancel,
            html:
                '<div style="text-align: left">' +
                '<form>' +
                '<div class="row" style="text-align: center; display: block">' +
                sprites[currentChar.image] +
                '</div>' +
                '  <div class="row">' +
                '<div class="col-6"> ' +
                '<div class="form-group row">' +
                '    <label for="firstname" class="col-3 col-form-label">' + trad.firstName + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="firstname" name="firstname" type="text" required="required" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="lastname" class="col-3 col-form-label">' + trad.lastName + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="lastname" name="lastname" value="" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6">  ' +
                '<div class="form-group row">' +
                '    <label for="birthdate" class="col-5 col-form-label">' + trad.birthdate + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="birthdate" name="birthdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="deathdate" class="col-5 col-form-label">' + trad.deathdate + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="deathdate" name="deathdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '  <div class="form-group row">' +
                '    <label for="address" class="col-2 col-form-label">' + trad.address + '</label> ' +
                '    <div class="col-10">' +
                '      <textarea id="address" name="address" cols="40" rows="2"  value="" class="form-control"></textarea>' +
                '    </div>' +
                '  </div>' +
                (displayParents ?
                        '  <div class="form-group row">' +
                        '    <label for="motherLastName" class="col-2 col-form-label">' + trad.mother + '</label> ' +
                        '    <div class="col-10">' +
                        '      <div class="input-group">' +
                        '        <div class="input-group-prepend">' +
                        '          <div class="input-group-text">' + trad.lastName + '</div>' +
                        '        </div> ' +
                        '        <input id="motherLastName" name="motherLastName" placeholder="" type="text" value="" class="form-control">' +
                        '        <div class="input-group-prepend">' +
                        '          <div class="input-group-text">' + trad.firstName + '</div>' +
                        '        </div> ' +
                        '        <input id="motherFirstName" name="motherFirstName" placeholder="" type="text"  value="" class="form-control">' +
                        '      </div>' +
                        '    </div>' +
                        '  </div>' +
                        '  <div class="form-group row">' +
                        '    <label for="fatherLastName" class="col-2 col-form-label">' + trad.father + '</label> ' +
                        '    <div class="col-10">' +
                        '      <div class="input-group">' +
                        '        <div class="input-group-prepend">' +
                        '          <div class="input-group-text">' + trad.lastName + '</div>' +
                        '        </div> ' +
                        '        <input id="fatherLastName" name="fatherLastName" placeholder="" type="text" class="form-control" value="">' +
                        '        <div class="input-group-prepend">' +
                        '          <div class="input-group-text">' + trad.firstName + '</div>' +
                        '        </div> ' +
                        '        <input id="fatherFirstName" name="fatherFirstName" placeholder="" type="text" class="form-control" value="">' +
                        '      </div>' +
                        '    </div>' +
                        '  </div>'
                        : ''
                ) +
                (isIntruder ? '  <div class="form-group row">' +
                    '    <label for="childLastName" class="col-2 col-form-label">' + trad.child + '</label> ' +
                    '    <div class="col-10">' +
                    '      <div class="input-group">' +
                    '        <div class="input-group-prepend">' +
                    '          <div class="input-group-text">' + trad.lastName + '</div>' +
                    '        </div> ' +
                    '        <input id="childLastName" name="childLastName" placeholder="" type="text" class="form-control" value="">' +
                    '        <div class="input-group-prepend">' +
                    '          <div class="input-group-text">' + trad.firstName + '</div>' +
                    '        </div> ' +
                    '        <input id="childFirstName" name="childFirstName" placeholder="" type="text" class="form-control" value="">' +
                    '      </div>' +
                    '    </div>' +
                    '  </div>'
                    : '') +
                '</form>' +
                '</div>',
            onBeforeOpen: function () {
                if (currentChar) {
                    $('#firstname').val(currentChar.firstName);
                    $('#lastname').val(currentChar.lastName);
                    $('#birthdate').val(stringToDate(currentChar.birthdate));
                    $('#deathdate').val(stringToDate(currentChar.deathdate));
                    $('#address').val(currentChar.address.replace(', ', '\n'));
                    if ($('#fatherFirstName').length) $('#fatherFirstName').val(currentChar.father.firstName);
                    if ($('#fatherLastName').length) $('#fatherLastName').val(currentChar.father.lastName);
                    if ($('#motherFirstName').length) $('#motherFirstName').val(currentChar.mother.firstName);
                    if ($('#motherLastName').length) $('#motherLastName').val(currentChar.mother.lastName);
                    if ($('#childFirstName').length) $('#childFirstName').val(currentChar.child.firstName);
                    if ($('#childLastName').length) $('#childLastName').val(currentChar.child.lastName);
                }

            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#firstname').val(),
                        $('#lastname').val(),
                        $('#birthdate').val(),
                        $('#deathdate').val(),
                        $('#address').val(),
                        $('#fatherFirstName').length ? $('#fatherFirstName').val() : '',
                        $('#fatherLastName').length ? $('#fatherLastName').val() : '',
                        $('#motherFirstName').length ? $('#motherFirstName').val() : '',
                        $('#motherLastName').length ? $('#motherLastName').val() : '',
                        $('#childFirstName').length ? $('#childFirstName').val() : '',
                        $('#childLastName').length ? $('#childLastName').val() : '',
                    ]);
                });
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 11) {
                character = {
                    lastName: result.value[1].trim(),
                    firstName: result.value[0].trim(),
                    birthdate: dateToString(result.value[2].trim()),
                    deathdate: dateToString(result.value[3].trim()),
                    address: result.value[4].replace('\n', ', ').trim(),
                    mother: displayParents ? {
                        firstName: result.value[7].trim(),
                        lastName: result.value[8].trim()
                    } : currentChar.mother,
                    father: displayParents ? {
                        firstName: result.value[5].trim(),
                        lastName: result.value[6].trim()
                    } : currentChar.mother,
                    child: {
                        firstName: result.value[10].trim(),
                        lastName: result.value[9].trim()
                    }
                };
                var checkResult = checkForError(character, child, displayParents, isIntruder);
                if (checkResult.code === 'OK') {
                    currentChar.lastName = character.lastName;
                    currentChar.firstName = character.firstName;
                    currentChar.birthdate = character.birthdate;
                    currentChar.deathdate = character.deathdate;
                    currentChar.address = character.address;
                    if (displayParents) {
                        currentChar.mother = character.mother;
                        currentChar.father = character.father;
                    }
                    if (isIntruder) {
                        currentChar.child = character.child;
                    }
                    buildTree();
                } else {
                    Swal.fire(
                        trad.error,
                        checkResult.error, 'error');
                }
            }
        }.bind(this));
    };

    function checkForError(character, child, noParents, isIntruder) {
        var checkResult = {
            code: 'OK',
            error: ''
        };
        if (!(character.lastName && character.lastName !== ''
            && character.firstName && character.firstName !== ''
            && character.birthdate && character.birthdate !== ''
            && character.address && character.address !== '')) {
            checkResult.code = 'error';
            checkResult.error = 'informationsMissing';
        } else if (noParents && !(character.mother.firstName && character.mother.firstName !== ''
            && character.mother.lastName && character.mother.lastName !== ''
            && character.father.firstName && character.father.firstName !== ''
            && character.father.lastName && character.father.lastName !== '')) {
            checkResult.code = 'error';
            checkResult.error = 'informationsMissing';
        } else if (isIntruder && !(character.child.lastName && character.child.lastName !== '' && character.child.firstName && character.child.firstName !== '')) {
            checkResult.code = 'error';
            checkResult.error = 'informationsMissing';
        } else {
            if (!noParents && !isIntruder) {
                var father = character.father;
                var mother = character.mother;
                //test mother
                if (calculateAge(mother.birthdate, '') < calculateAge(character.birthdate, '') + 10) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForMother';
                }
                //test father
                else if (calculateAge(father.birthdate, '') < calculateAge(character.birthdate, '') + 10) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForFather';
                }
            }
            if (child.birthdate && !isIntruder) {
                if (calculateAge(child.birthdate, '') > calculateAge(character.birthdate, '') - 10) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooYoungForChild';
                }
            }
        }
        checkResult.error = checkResult.code !== 'OK' ? trad[checkResult.error] : '';
        return checkResult;
    }

    function calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        var birthdate = new Date(birthDateString);
        var age;
        var month;
        if (deathDateString && deathDateString !== '') {
            deathDateString = deathDateString.split('.').reverse().join('-');
            var deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        } else {
            var today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        }
    }

    function dateToString(date) {
        if (!date || date === '')
            return '';
        return date.split('-').reverse().join('.');
    }

    function stringToDate(string) {
        if (!string || string === '')
            return '';
        return string.split('.').reverse().join('-');
    }


    const saveAndExit = function () {
        const levelInput = $('#niveauExo');
        const textArea = $('#descriptionExo');
        const imageB64 = $('#img_preview').attr('src');
        // L'indication du niveau de l'exercice est obligatoire
        if (levelInput.val()===''){
            levelInput.css('border-color','red');
            levelInput.focus();
        }
        else{
            wrk.send('/updateExercice', false, {exercice: JSON.stringify(exercice), exId: exId}, function (data, success) {
                if (success) {
                    wrk.send('/updateExerciceState', false, {description:textArea.val(),state: 2, levelInput:levelInput.val(), imageB64, exId: exId}, function (data, success) {
                        Swal.fire({
                            title: data.message,
                            icon: data.status,
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            $('#returnBtn').click();
                        });
                    }, true);
                } else {
                    Swal.fire({
                        title: data.message,
                        icon: data.status,
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    });
                }
            }, true);
        }
    };

    const _delete = function (id) {
        wrk.send('/admin/valider/supprimer', false, {exercice: id}, function (response, success) {
            $('#returnBtn').click();
        }, true);
    };


    function fillSprites(data) {
        data = JSON.parse(data);
        for (var sprite of data) {
            var html = '<img src="data:image/png;base64,' + sprite.base64 + '" height="70" width="25" style="margin-bottom: 20px">';
            sprites[sprite.nom] = html;
        }
    }

    return {
        buildTree,
        displayInfos,
        saveAndExit,
        delete: _delete
    };
})();

