const json_per_brut = `{ "disciplines": [
        {
            "id": 1,
            "nom": "Français",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 1,
                            "nom": "Lire et écrire des textes d'usage familier et scolaire et s'approprier le système de la langue écrite",
                            "code": "L1 11-12",
                            "cycle": 1
                        },
                        {
                            "id": 3,
                            "nom": "Lire de manière autonome des textes variés et développer son efficacité en lecture",
                            "code": "L1 21",
                            "cycle": 2
                        },
                        {
                            "id": 7,
                            "nom": "Lire et analyser des textes de genres différents et en dégager les multiples sens",
                            "code": "L1 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        },
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        },
                        {
                            "id": 8,
                            "nom": "Écrire des textes de genres différents adaptés aux situations d’énonciation",
                            "code": "L1 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 2,
                            "nom": "Comprendre et produire des textes oraux d'usage familier et scolaire",
                            "code": "L1 13-14",
                            "cycle": 1
                        },
                        {
                            "id": 5,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 23",
                            "cycle": 2
                        },
                        {
                            "id": 5,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 23",
                            "cycle": 2
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 2,
                            "nom": "Comprendre et produire des textes oraux d'usage familier et scolaire",
                            "code": "L1 13-14",
                            "cycle": 1
                        },
                        {
                            "id": 6,
                            "nom": "Produire des textes oraux variés propres à des situations de la vie courante",
                            "code": "L1 24",
                            "cycle": 2
                        },
                        {
                            "id": 10,
                            "nom": "Produire des textes oraux de genres différents adaptés aux situations d'énonciation",
                            "code": "L1 34",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 5,
                    "nom": "Accès à la littérature",
                    "objectifs":[
                        {
                            "id": 11,
                            "nom": "Apprécier des ouvrages littéraires",
                            "code": "L1 15",
                            "cycle": 1
                        },
                        {
                            "id": 15,
                            "nom": "Conduire et apprécier la lecture d'ouvrages littéraires",
                            "code": "L1 25",
                            "cycle": 2
                        },
                        {
                            "id": 19,
                            "nom": "Apprécier et analyser des productions littéraires diverses",
                            "code": "L1 35",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 12,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L1 16",
                            "cycle": 1
                        },
                        {
                            "id": 16,
                            "nom": "Construire une représentation de la langue pour comprendre et produire des textes",
                            "code": "L1 26",
                            "cycle": 2
                        },
                        {
                            "id": 20,
                            "nom": "Analyser le fonctionnement de la langue et élaborer des critères d'appréciation pour comprendre et produire des textes",
                            "code": "L1 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 13,
                            "nom": "Identifier l'organisation et le fonctionnement de la langue par l'observation et la manipulation d'autres langues",
                            "code": "L 17",
                            "cycle": 1
                        },
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 8,
                    "nom": "Écriture et instruments de la communication",
                    "objectifs":[
                        {
                            "id": 14,
                            "nom": "Découvrir et utiliser la technique de l'écriture et les instruments de la communication",
                            "code": "L1 18",
                            "cycle": 1
                        },
                        {
                            "id": 18,
                            "nom": "Utiliser l'écriture et les instruments de la communication pour planifier et réaliser des documents",
                            "code": "L1 28",
                            "cycle": 2
                        },
                        {
                            "id": 153,
                            "nom": "Exploiter l'écriture et les instruments de la communication pour collecter l'information, pour échanger et pour produire les documents",
                            "code": "L1 38",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 3,
            "nom": "Allemand",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 21,
                            "nom": "Lire des textes propres à des situations familières de communication",
                            "code": "L2 21",
                            "cycle": 2
                        },
                        {
                            "id": 25,
                            "nom": "Lire de manière autonome des textes rédigés en langage courant",
                            "code": "L2 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 22,
                            "nom": "écrire des textes simples propres à des situations familières de communication",
                            "code": "L2 22",
                            "cycle": 2
                        },
                        {
                            "id": 27,
                            "nom": "écrire des textes variés sur des sujets familiers ou d'intérêt personnel",
                            "code": "L2 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 23,
                            "nom": "Comprendre des textes oraux brefs propres à des situations familières de communication",
                            "code": "L2 23",
                            "cycle": 2
                        },
                        {
                            "id": 33,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 26",
                            "cycle": 2
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 24,
                            "nom": "Produire des énoncés simples propres à des situations familières de communication",
                            "code": "L2 24",
                            "cycle": 2
                        },
                        {
                            "id": 34,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 33,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 26",
                            "cycle": 2
                        },
                        {
                            "id": 34,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L2 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 4,
            "nom": "Anglais",
            "competences": [
                {
                    "id": 1,
                    "nom": "Compréhension de l'écrit",
                    "objectifs":[
                        {
                            "id": 173,
                            "nom": "Lire des textes propres à des situations familières de communication",
                            "code": "L3 21",
                            "cycle": 2
                        },
                        {
                            "id": 26,
                            "nom": "Lire de manière autonome des textes rédigés en langage courant",
                            "code": "L3 31",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 2,
                    "nom": "Production de l'écrit",
                    "code": "",
                    "cycle": 1,
                    "objectifs":[
                        {
                            "id": 175,
                            "nom": "écrire des textes simples propres à des situations familières de communication",
                            "code": "L3 22",
                            "cycle": 2
                        },
                        {
                            "id": 180,
                            "nom": "écrire des textes variés sur des sujets familiers ou d'intérêt personnel",
                            "code": "L3 32",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 3,
                    "nom": "Compréhension de l'oral",
                    "objectifs":[
                        {
                            "id": 172,
                            "nom": "Comprendre des textes oraux brefs propres à des situations familières de communication",
                            "code": "L3 23",
                            "cycle": 2
                        },
                        {
                            "id": 30,
                            "nom": "Comprendre des textes oraux variés propres à des situations de la vie courante",
                            "code": "L3 33",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 4,
                    "nom": "Production de l'oral",
                    "objectifs":[
                        {
                            "id": 174,
                            "nom": "Produire des énoncés simples propres à des situations familières de communication",
                            "code": "L3 24",
                            "cycle": 2
                        },
                        {
                            "id": 32,
                            "nom": "Produire des textes oraux variés propres à des situations de la vie courante",
                            "code": "L3 34",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 6,
                    "nom": "Fonctionnement de la langue",
                    "objectifs":[
                        {
                            "id": 176,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L3 26",
                            "cycle": 2
                        },
                        {
                            "id": 181,
                            "nom": "Observer le fonctionnement de la langue et s'approprier des outils de base pour comprendre et produire des textes",
                            "code": "L3 36",
                            "cycle": 3
                        }
                    ]
                },
                {
                    "id": 7,
                    "nom": "Approches interlinguistiques",
                    "objectifs":[
                        {
                            "id": 17,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 27",
                            "cycle": 2
                        },
                        {
                            "id": 158,
                            "nom": "Enrichir sa compréhension et sa pratique langagière par l'établissement de liens avec des langues différentes",
                            "code": "L 37",
                            "cycle": 3
                        }
                    ]
                }
            ]
        },
        {
            "id": 2,
            "nom": "Latin",
            "competences": []
        }
    ]
}`;

module.exports = json_per_brut;