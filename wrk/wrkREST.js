/**
 @classdesc Worker communiquant avec le serveur REST
 @author Kolly Florian
 @version 1.0
 **/

// -------------- requêtes LRS --------------

/**
 * Requête async. vers le système LRS du LIRIS
 */
const sendDatatoLRS = async (exoId, exoName) => {
    try {
        return await axios.post('https://comper.projet.liris.cnrs.fr/sites/plateform-to-ref/linkResource.php', querystring.stringify({
            id:exoId,
            name:exoName
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Pas de réponse du serveur : ${e.message}`);
    }
};


// -------------- UTILISATEUR --------------

/**
 * Requête async. sur le REST pour vérifier le login
 * @param  {string}  email    l'email du professeur
 * @param  {string}  password le mot de passe du professeur
 * @return {Promise}          resolve si pas d'erreur lors de la requête, reject autrement
 */
const verifyLogin = async (email, password) => {
    try {
        return await axios.post(`${RESTFUL}/login`, querystring.stringify({
            email,
            password,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour créer un compte
 * @param  {string}  email    l'email du professeur
 * @param  {string}  password le mot de passe du professeur
 * @return {Promise}          resolve si pas d'erreur lors de la requête, reject autrement
 */
const doSignup = async (email, password, code) => {
    try {
        return await axios.post(`${RESTFUL}/signup`, querystring.stringify({
            email,
            password,
            code,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour ajouter un code à un professeur
 * @param  {string}  email l'email du professeur
 * @param  {string}  code  le code à ajouter
 * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
 */
const addCodeToProfesseur = async (email, code) => {
    try {
        return await axios.put(`${RESTFUL}/addCodeToProfesseur`, querystring.stringify({
            email,
            code,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour vérifier le code d'un professeur
 * @param  {string}  code  le code à vérifier
 * @param  {string}  email l'email du professeur entrant le code
 * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
 */
const verifyCodeForRecovery = async (code, email) => {
    try {
        return await axios.post(`${RESTFUL}/verifyCodeForRecovery`, querystring.stringify({
            code,
            email,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour modifier le mot de passe d'un professeur
 * @param  {string}  email    l'email du professeur
 * @param  {string}  password le nouveau mot de passe
 * @return {Promise}          resolve si pas d'erreur lors de la requête, reject autrement
 */
const modifyPassword = async (email, password) => {
    try {
        return await axios.put(`${RESTFUL}/modifyPassword`, querystring.stringify({
            email,
            password,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

// -------------- CLASSE --------------

/**
 * Requête async. sur le REST pour ajouter une classe à un professeur
 * @param  {string}  nom    le nom de la classe
 * @param  {number}  degre  l'id du degré Harmos
 * @param  {number}  id     l'id du professeur
 * @param  {string}  eleves la liste des élèves, sous forme de string, séparés par un %
 * @return {Promise}        resolve si pas d'erreur lors de la requête, reject autrement
 */
const addClasse = async (nom, degre, id, eleves) => {
    try {
        return await axios.post(`${RESTFUL}/addClasse`, querystring.stringify({
            nom,
            degre,
            id,
            eleves,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour ajouter une classe partagée aux classes du professeur indiqué
 * @param  {number}  id   l'id du professeur à qui ajouter la classe
 * @param  {string}  uuid l'uuid de la classe à ajouter
 * @return {Promise}      resolve si pas d'erreur lors de la requête, reject autrement
 */
const shareClasse = async (id, uuid) => {
    try {
        return await axios.put(`${RESTFUL}/shareClasse`, querystring.stringify({
            id,
            uuid,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

// -------------- ELEVE --------------

/**
 * Requête async. POST sur le REST avec un paramètre ID uniquement
 * @param  {string}  route la route de la requête
 * @param  {number}  id    l'ID de l'objet cherché
 * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
 */
const getEleve = async (id) => {
    try {
        return await axios.post(`${RESTFUL}/getEleve`, querystring.stringify({id, salt: util.getCommunicationSalt()}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour ajouter un élève à une classe
 * @param  {string}  nom    le nom de l'élève
 * @param  {number}  classe l'id de la classe
 * @return {Promise}        resolve si pas d'erreur lors de la requête, reject autrement
 */
const addEleve = async (nom, classe) => {
    try {
        return await axios.post(`${RESTFUL}/addEleve`, querystring.stringify({
            nom,
            classe,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

// -------------- PARCOURS --------------

    /**
     * Requête async. POST sur le REST avec un paramètre ID uniquement
     * @param  {String} target    l'ID de l'objet cherché
     * @param  {int}  targetId la route de la requête
     * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
     */
    const getAllParcoursOfTarget = async (target,targetId) => {
        try {
            return await axios.post(`${RESTFUL}/getAllParcoursOfTarget`, querystring.stringify({target,targetId}));
        } catch (e) {
            console.error(e.response.data);
            throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
        }
    };

    /**
     * Obtient les informations d'un seul parcours à partir du userId et du nomParcours (utile si on n'a pas l'id)
     * @param  {int}    userId l'id de l'utilisateur / le prof
     * @param  {String} nomParcours le nom du parcours (unique pour chauqe userId)
     * @return {Promise}   resolve si pas d'erreur lors de la requête, reject autrement
     */
    const getSingleParcoursByUserAndName = async (userId,nomParcours) => {
        try {
            return await axios.post(`${RESTFUL}/getSingleParcoursByUserAndName`, querystring.stringify({userId,nomParcours}));
        } catch (e) {
            console.error(e.response.data);
            throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
        }
    };

    /**
     * Vérifie si le parcours existe pour une classe ou un élève donné
     * @param {String} target pour savoir si on fait la requete sur une classe ou sur un élève
     * @param  {int}    targetId  id de la calsse ou de l'élève
     * @param  {int}    parcoursId l'id du parcours
     * @return {Promise}   resolve si pas d'erreur lors de la requête, reject autrement
     */
    const checkParcoursFor = async (target,targetId,parcoursId) => {
        try {
            return await axios.post(`${RESTFUL}/checkParcoursFor`, querystring.stringify({target, targetId, parcoursId}));
        } catch (e) {
            console.error(e.response.data);
            throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
        }
    };

/**
 * Requête async. sur le REST pour ajouter un parcours à une classe ou à un élève
 * @param {String} target pour savoir si on fait la requete sur une classe ou sur un élève
 * @param {int} targetId l'id de la classe qui va recevoir le parcours
 * @param {int} parcoursId l'id du parcours
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const saveParcoursTo = async (target, targetId, parcoursId) => {
    try {
        return await axios.post(`${RESTFUL}/saveParcoursTo`, querystring.stringify({
            target,
            targetId,
            parcoursId,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour supprimer un parcours d'une classe ou d'un élève
 * @param {String} target pour savoir si on fait la requete sur une classe ou sur un élève
 * @param {int} targetId l'id de la classe qui va recevoir le parcours
 * @param {int} parcoursId l'id du parcours
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const deleteParcoursFrom = async (target, targetId, parcoursId) => {
    try {
        return await axios.post(`${RESTFUL}/deleteParcoursFrom`, querystring.stringify({
            target,
            targetId,
            parcoursId,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour supprimer un parcours pour un professeur
 * @param {int} parcoursId l'id du parcours
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const deleteParcoursWithId = async (parcoursId) => {
    try {
        return await axios.post(`${RESTFUL}/deleteParcoursWithId`, querystring.stringify({
            parcoursId,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. sur le REST pour ajouter un parcours dans la base
 * @param  {int} user_id le nom du parcours
 * @param  {String} nom le nom du parcours
 * @param  {String} licence le type de licence du parcours
 * @param  {String} description la description du parcours
 * @param  {int} degre le degre Harmos du parcours
 * @param  {String} competences les comeptences du parcours (json à la base)
 * @param  {String} steps les étapes du parcours (json à la base)
 * @param  {String} requestURL pour savoir si c'est un insert ou un update
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const saveParcours = async (user_id,nom,licence,description, degre,competences,steps,requestURL) => {
    try {
        return await axios.post(`${RESTFUL}/`+requestURL, querystring.stringify({
            user_id,
            nom,
            licence,
            description,
            degre,
            competences,
            steps,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Modifie le statut d'un parcours (privé / en attente / public)
 * @param {int} id l'id du parcours
 * @param {int} status la valeur du statut : 0=privé, 1 = en attente de publication, 2 = public
 */
const updateParcoursStatus = async (id,status) => {
    try {
        return await axios.post(`${RESTFUL}/updateParcoursStatus`, querystring.stringify({
            id,
            status,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Met à jour l'état de l'étape dans le json
 * @param {int} parcoursId l'id du parcours à mettre à jour
 * @param {String} stepsJson le json des étapes du parcours
 */
const updateStepsJson = async (parcoursId,stepsJson) => {
    try {
        return await axios.post(`${RESTFUL}/updateStepsJson`, querystring.stringify({
            parcoursId,
            stepsJson,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getProfile = async (id, profile) => {
    try {
        return await axios.post(`${RESTFUL}/${profile}`, querystring.stringify({
            id,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error('Le coquin de serveur n\'a pas voulu nous donner de réponse: ' + e.message);
    }
};

const updateEleve = async (id, name) => {
    try {
        return await axios.put(`${RESTFUL}/updateEleve`, querystring.stringify({
            id,
            name,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error('Le coquin de serveur n\'a pas voulu nous donner de réponse: ' + e.message);
    }
};

// -------------- JEU --------------

/**
 * Requête async. retournant les informations par rapport à un jeu via sa PK
 * @param  {number}  id la PK du jeu souhaité
 * @return {Promise}    resolve si pas d'erreur lors de la requête, reject autrement
 */
const getJeuByID = async (id) => {
    try {
        return await axios.post(`${RESTFUL}/getJeu`, querystring.stringify({id: parseInt(id)}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

// --------------STATISTIQUES------------

/**
 * Requête async. pour ajouter une statistique
 * @param {number} mode Le mode de jeu
 * @param {number} idJeu L'id du jeu
 * @param {number} idEleve L'id de l'élève
 * @param {number} exerciceId L'id de l'exercice en question
 * @param {number} parcoursId L'id du parcours en question si l'on est en mode Parcours
 */
const addStats = async (mode, idJeu, idEleve,exerciceId,parcoursId) => {
    try {
        return await axios.post(`${RESTFUL}/addStats`, querystring.stringify({
            mode,
            idJeu,
            idEleve,
            exerciceId,
            parcoursId,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. pour mettre à jour une statistique
 * @param {number} mode Le mode de jeu
 * @param {number} idJeu L'id du jeu
 * @param {number} idEleve L'id de l'élève
 */
const updateStats = async (id, evaluation) => {
    try {
        return await axios.post(`${RESTFUL}/updateStats`, querystring.stringify({
            id,
            evaluation,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

// -------------- EXERCICE --------------

/**
 * Requête async. retournant un exercice par rapport au jeu et au niveau harmos séléctionné
 * @method getExercice
 * @param  {number}    id    la PK du jeu
 * @param  {number}    degre le degre Harmos
 * @return {Promise}         resolve si pas d'erreur lors de la requête, reject autrement
 */
const getExercice = async (id, degre) => {
    try {
        return await axios.post(`${RESTFUL}/getExercice`, querystring.stringify({id, degre}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. retournant tous les exercices d'un jeu séléctionné
 * @method getAllExercices
 * @param  {number}    id    la PK du jeu
 * @param  {number}    degre le degre Harmos
 * @return {Promise}         resolve si pas d'erreur lors de la requête, reject autrement
 */
const getAllExercices = async () => {
    try {
        return await axios.get(`${RESTFUL}/getAllExercices`);
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. retournant tous les exercices d'un seul parcours à travers les identifiants
 * @method getParcoursExercices
 * @param  {number[]} stepsId le tableau d'entiers
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const getParcoursExercices = async (stepsId) => {
    try {
        return await axios.post(`${RESTFUL}/getParcoursExercices`, querystring.stringify({stepsId,salt: util.getCommunicationSalt()}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getExerciceEleves = async (id, degre) => {
    try {
        return await axios.post(`${RESTFUL}/getExerciceEleves`, querystring.stringify({id, degre}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getExericesForValidation = async () => {
    try {
        return await axios.post(`${RESTFUL}/getExericesForValidation`, querystring.stringify({}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. retournant un exercice précis à travers son id
 * @method getExercice
 * @param  {number}    exId  l'id de l'exercice dans la table t_exercice en BD
 * @return {Promise}         resolve si pas d'erreur lors de la requête, reject autrement
 */
const getExericesById = async (exId) => {
    try {
        return await axios.post(`${RESTFUL}/getExericesById`, querystring.stringify({exId: exId}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getExerciceById = async (forcedExerciceId) => {
    try {
        return await axios.post(`${RESTFUL}/getExerciceById`, querystring.stringify({forcedExerciceId: forcedExerciceId}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getUsers = async () => {
    try {
        return await axios.get(`${RESTFUL}/getUsers`);
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const updateRole = async (userId, role) => {
    try {
        return await axios.post(`${RESTFUL}/updateRole`, querystring.stringify({userId: userId, role: role,salt: util.getCommunicationSalt()}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const addExercice = async (exercice, fkJeu, eleve, degre,forcedState) => {
    try {
        return await axios.post(`${RESTFUL}/addExercice`, querystring.stringify({
            exercice,
            fkJeu,
            eleve,
            degre,
            forcedState
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Ajoute un nouveau exercice débranché
 */
const createUnpluggedActivity = async (exercice, fkJeu, eleve, exoName) => {
    try {
        return await axios.post(`${RESTFUL}/createUnpluggedActivity`, querystring.stringify({
            salt: util.getCommunicationSalt(),
            exercice,
            fkJeu,
            eleve,
            exoName
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Met à jour le step dans la base de données (Modifie un exercice débranché déjà existant)
 * @param {int} stepId l'id du step
 * @param {string} content le contenu à assigner au step
 */
const updateUnpluggedExercice = async (stepId,content) => {
    try {
        return await axios.post(`${RESTFUL}/updateUnpluggedExercice`, querystring.stringify({
            stepId,
            content
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

    // -------------- GLOBAL --------------

/**
 * Requête async. GET sur le REST
 * @param  {string}  route la route de la requête
 * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
 */
const requestGETfromREST = async (route) => {
    try {
        return await axios.get(`${RESTFUL}/${route}`);
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Requête async. POST sur le REST avec un paramètre ID uniquement
 * @param  {string}  route la route de la requête
 * @param  {number}  id    l'ID de l'objet cherché
 * @return {Promise}       resolve si pas d'erreur lors de la requête, reject autrement
 */
const requestPOSTfromRESTWithID = async (route, id) => {
    try {
        return await axios.post(`${RESTFUL}/${route}`, querystring.stringify({id, salt: util.getCommunicationSalt()}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 *  Vérifie si le mot de passe du prof est bon
 * @param {number} password Le mot de passe du prof
 * @param {number} prof L'id du professeur
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const checkPasswordForId = async (password, prof) => {
    try {
        return await axios.post(`${RESTFUL}/checkPasswordForId`, querystring.stringify({
            password,
            prof,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 *
 * @param {number} ex L'id de l'exercice signalé
 * @param {number} prof L'id du professeur qui signal
 * @return {Promise} resolve si pas d'erreur lors de la requête, reject autrement
 */
const signaler = async (msg, password, ex, prof) => {
    try {
        return await axios.post(`${RESTFUL}/signaler`, querystring.stringify({
            ex,
            prof,
            msg,
            password,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getExerciceSignale = async () => {
    try {
        return await axios.post(`${RESTFUL}/getExerciceSignale`, querystring.stringify({
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const updateExercice = async (exercice, levelInput, exId) => {
    try {
        return await axios.post(`${RESTFUL}/updateExercice`, querystring.stringify({
            salt: util.getCommunicationSalt(),
            exercice: exercice,
            levelInput:levelInput,
            exId: exId
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Obtient les informations d'un seul parcours à partir de son ID
 * @param  {String} stepsId tous les Id des steps du parcours
 * @return {Promise}   resolve si pas d'erreur lors de la requête, reject autrement
 */
const getStepsInfo = async (stepsId) => {
    try {
        return await axios.post(`${RESTFUL}/getStepsInfo`, querystring.stringify({
            stepsId:stepsId,
            salt: util.getCommunicationSalt(),}));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const updateExerciceState = async (description,state, levelInput, imageB64,exId) => {
    try {
        return await axios.post(`${RESTFUL}/updateExerciceState`, querystring.stringify({
            salt: util.getCommunicationSalt(),
            description,
            state: state,
            levelInput: levelInput,
            imageB64: imageB64,
            exId: exId
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const deleteExercice = async (exId) => {
    try {
        return await axios.post(`${RESTFUL}/deleteExercice`, querystring.stringify({
            salt: util.getCommunicationSalt(),
            exId: exId
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

/**
 * Réinitialise un parcours pour un élève en supprimant les historiques de ses exercices dans le parcours
 * @param  {String} studentId l'id de l'élève
 * @param  {String} parcoursId l'id du parcours
 * @return {Promise}   resolve si pas d'erreur lors de la requête, reject autrement
 */
const resetParcoursForStudent = async (studentId,parcoursId) => {
    try {
        return await axios.post(`${RESTFUL}/resetParcoursForStudent`, querystring.stringify({
            studentId : studentId,
            parcoursId : parcoursId,
            salt: util.getCommunicationSalt()
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const getSprites = async (gameId) => {
    try {
        return await axios.post(`${RESTFUL}/getSprites`, querystring.stringify({
            salt: util.getCommunicationSalt(),
            gameId: gameId
        }));
    } catch (e) {
        console.error(e.response.data);
        throw new Error(`Le coquin de serveur n'a pas voulu nous donner de réponse: ${e.message}`);
    }
};

const returnSalt = () => {
   // return util.getCommunicationSalt();
    return '';
};

module.exports = {
    verifyLogin,
    doSignup,
    addCodeToProfesseur,
    createUnpluggedActivity,
    verifyCodeForRecovery,
    modifyPassword,
    addClasse,
    shareClasse,
    getEleve,
    addEleve,
    getProfile,
    updateEleve,
    addStats,
    updateStats,
    getJeuByID,
    getExercice,
    getExerciceEleves,
    getExericesForValidation,
    updateRole,
    getUsers,
    addExercice,
    requestGETfromREST,
    requestPOSTfromRESTWithID,
    signaler,
    getExerciceSignale,
    getExerciceById,
    getExericesById,
    updateExercice,
    updateExerciceState,
    updateStepsJson,
    updateParcoursStatus,
    updateUnpluggedExercice,
    deleteExercice,
    returnSalt,
    getSprites,
    getAllExercices,
    getParcoursExercices,
    saveParcours,
    saveParcoursTo,
    sendDatatoLRS,
    deleteParcoursFrom,
    deleteParcoursWithId,
    getSingleParcoursByUserAndName,
    checkParcoursFor,
    checkPasswordForId,
    getAllParcoursOfTarget,
    getStepsInfo,
    resetParcoursForStudent
};
